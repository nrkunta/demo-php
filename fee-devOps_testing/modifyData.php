<?php
include_once('api/xml2array.php');
include_once("api/domxml-php4-to-php5.php");
include_once("admin/inc/db_config.php");
include_once("inc/config.php");
include_once('api/api_prop.php');

$serviceDependentId=$dependentId = "00"; 
$REQDATE = date("Ymd");
$deployment = sanitize_data_email($_POST['deployment']);
$strCaseNumber = sanitize_data($_POST['zreqnum']);
$ZREQNUM = $strCaseNumber;
$policyNumber = sanitize_data($_POST['policyNumber']);
$endorsmentDropDown = sanitize_data($_POST['endorsmentType']);
$CLNTNUM = sanitize_data($_POST['clntnum']);
$dependentId = sanitize_data($_POST['dependentId']);
$endorsement_done_by = sanitize_data($_POST['endorsement_done_by']);
$agent_id = sanitize_data_email($_POST['agent_id']);
$policy_status = sanitize_data($_POST['policy_status']);
$policy_effective_date = sanitize_data($_POST['policy_effective_date']);
$product_type = sanitize_data($_POST['product_type']);
$old_value = sanitize_data_address($_POST['old_value']);
$new_value = sanitize_data_address($_POST['new_value']);
$session_id = sanitize_data($_POST['session_id']);
$resMemberId =sanitize_data($_POST['member_no']);
$responseData = array();
//CSRF Validation check  
$csrf= sanitize_data(@$_POST['token']);
if(isset($csrf)){
    if($csrf!=$_SESSION["token"]){
        echo $error = "1## CSRF Validation failed";exit;
    }
}
//get product type from request for crm only
if($deployment == 'crm'){
 $resultData = file_get_contents("data/renewal/" . $policyNumber . "-" . $endorsmentDropDown . "_Response.xml");
 $xml = new xml2array($resultData);
 $dataArr = $xml->getResult();
 $data = @$dataArr['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['policy'];	
 $product_type =  strtolower($data['base-product-family-cd']['#text']);	
}

//dependentID
if($endorsmentDropDown==15){
  $serviceDependentId = $dependentId;
}
//PROP-4024
if($product_type=='Health'){ /* to check policy type */
/*  For current date policy  */
if(strtotime($policy_effective_date)<=strtotime($REQDATE)){  

$policy_effective_date=$REQDATE;

}elseif(strtotime($policy_effective_date)>strtotime($REQDATE)){ /* For Future date policy */
	
	$policy_effective_date=$policy_effective_date;
}
} else {
	
	$policy_effective_date = sanitize_data($_POST['policy_effective_date']);
}
// start jira id: FAV-1747
$xmlReq  = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:rel="http://relinterface.insurance.symbiosys.c2lbiz.com" xmlns:xsd="http://intf.insurance.symbiosys.c2lbiz.com/xsd">
   <soap:Header/>
   <soap:Body>
      <rel:PolicyEndorsement>
       <rel:intIO>
	        <xsd:agentID>' . $agent_id . '</xsd:agentID>
			<xsd:userIP>'. $_SERVER['REMOTE_ADDR'].'</xsd:userIP>
			<xsd:caseEffDate>' . $policy_effective_date . '</xsd:caseEffDate>
            <xsd:caseID>' . $ZREQNUM . '</xsd:caseID>
            <xsd:customerID>'.$CLNTNUM.'</xsd:customerID>
            <xsd:dependentNo>'.$serviceDependentId.'</xsd:dependentNo>
			<xsd:doneBy>'.$endorsement_done_by.'</xsd:doneBy>
            <xsd:endorsementType>' . $endorsmentDropDown . '</xsd:endorsementType>
			<xsd:sessionID>' . $session_id . '</xsd:sessionID>';
			if ($endorsmentDropDown == '23') {
				$xmlReq .= '<xsd:maritalStatusCd>' . $new_value .'</xsd:maritalStatusCd>';
			}
			if ($endorsmentDropDown == '16') {
				$xmlReq .= '<xsd:motherMaidenName>' . $new_value .'</xsd:motherMaidenName>';
			}
			
            $xmlReq .= '<xsd:memberNo>'.$resMemberId.'</xsd:memberNo>
						<xsd:newValue>' . $new_value . '</xsd:newValue>
						<xsd:oldValue>' . $old_value . '</xsd:oldValue>
						<xsd:policyStatus>' . $policy_status . '</xsd:policyStatus>
						<xsd:policyType>' . $product_type . '</xsd:policyType>
						<xsd:source>' . $deployment . '</xsd:source>
						<xsd:policyDetails>
							<xsd:policy>
								<xsd:partyDOList>';
								
								    //policyholder name
									if (($endorsmentDropDown == '01') || ($endorsmentDropDown == '15')) {
										$newvalue_arr = explode(',' , $new_value);
										
										if($newvalue_arr[1]==''){
											$newvalue_arr[1]=".";
										}
										

										$xmlReq .= '<xsd:firstName>' . @$newvalue_arr[0] . '</xsd:firstName>
													 <xsd:lastName>' . @$newvalue_arr[1]  . '</xsd:lastName>';
									}
									
									//address
									if ($endorsmentDropDown == '08') {
										$newvalue_arr = explode('@$' , $new_value);
										//if values is empty  passing n/a value as a part of address change issue-prop3650 
										if($newvalue_arr[2]==''){
											$newvalue_arr[2]="N/A";
										}
										$xmlReq .= '<xsd:partyAddressDOList>
											<xsd:addressLine1Lang1>' . @$newvalue_arr[0] . '</xsd:addressLine1Lang1>
											<xsd:addressLine2Lang1>' . @$newvalue_arr[1] . '</xsd:addressLine2Lang1>
											<xsd:areaCd>' . @$newvalue_arr[2] . '</xsd:areaCd>
											<xsd:cityCd>' . @$newvalue_arr[3] . '</xsd:cityCd>
											<xsd:stateCd>' . @$newvalue_arr[4] . '</xsd:stateCd>
											<xsd:pinCode>' . @$newvalue_arr[5] . '</xsd:pinCode>
										 </xsd:partyAddressDOList>';
									}  
									
									//phone no
									if ($endorsmentDropDown == '11') {
										$xmlReq .= '<xsd:partyContactDOList>
														<xsd:contactNum>'.$new_value.'</xsd:contactNum>
													</xsd:partyContactDOList>';
									}    
									
									//email
									if ($endorsmentDropDown == '12') {
										$xmlReq .= '<xsd:partyEmailDOList>
														<xsd:emailAddress>'.$new_value.'</xsd:emailAddress>
													</xsd:partyEmailDOList>';
									}
									
									//pan no
									if ($endorsmentDropDown == '20') {
										$xmlReq .= '<xsd:partyIdentityDOList>
														<xsd:identityNum>'.$new_value.'</xsd:identityNum>
													 </xsd:partyIdentityDOList>';
									}
									$xmlReq .= '</xsd:partyDOList>
									<xsd:policyAdditionalFieldsDOList>';
									
									//course details
									if ($endorsmentDropDown == '32') {
										$xmlReq .= '<xsd:courseDetails>'.$new_value.'</xsd:courseDetails>';
									}
									
									//university details
									if ($endorsmentDropDown == '31') {
										$newvalue_arr = explode(',' , $new_value);
										$xmlReq .=  '<xsd:field8>'.$newvalue_arr[0].'</xsd:field8>
										            <xsd:universityAddress>'.$newvalue_arr[1].'</xsd:universityAddress>';
									}
									
									//sponsor details
									if ($endorsmentDropDown == '30') {
										$newvalue_arr = explode(',' , $new_value);
										$xmlReq .=  '<xsd:sponsorName>'.$newvalue_arr[0].'</xsd:sponsorName>
										            <xsd:sponsorDOB>'.$newvalue_arr[1].'</xsd:sponsorDOB>
													 <xsd:relationshipToStudent>'.$newvalue_arr[2].'</xsd:relationshipToStudent>';
									}
									
									//nominee details
									if ($endorsmentDropDown == '05') {
										$newvalue_arr = explode(',' , $new_value);
										$xmlReq .=  '<xsd:field9>'.$newvalue_arr[0].'</xsd:field9>
										             <xsd:field10>'.$newvalue_arr[1].'</xsd:field10>
													 <xsd:field11>'.$newvalue_arr[2].'</xsd:field11>
													 <xsd:field17>'.$newvalue_arr[3].'</xsd:field17>
													 <xsd:field12>'.$newvalue_arr[4].'</xsd:field12>';
									}
									$xmlReq .= '</xsd:policyAdditionalFieldsDOList>
								<xsd:policyNum>'. $policyNumber . '</xsd:policyNum>
							</xsd:policy>
						</xsd:policyDetails>       
        </rel:intIO>
      </rel:PolicyEndorsement>
   </soap:Body>
</soap:Envelope>';



 				
file_put_contents("data/propero-log/" . $policyNumber . '-' . $endorsmentDropDown . "_Request.xml", $xmlReq);

//$start_time=microtime(true);

$response = soapReq($xmlReq ,'PolicyEndorsement');
if(empty($response)){
	echo '1## Sorry propero webservice not available right now.Please try later';
	exit;
}
$result =  new  xml2array($response);   

/*$end_time=microtime(true);
$diff=$end_time-$start_time;
echo $diff;
exit;
*/

file_put_contents("data/propero-log/" . $policyNumber . '-' . $endorsmentDropDown . "_Response.xml", $response);
$resultData=$result->getResult();				

if (isset($resultData['soapenv:Envelope']['soapenv:Body']['ns:PolicyEndorsementResponse']['ns:return']['int-endorsement-details-iO']['error-lists']['err-description']['#text'])) { 
    $error = '1##'.$resultData['soapenv:Envelope']['soapenv:Body']['ns:PolicyEndorsementResponse']['ns:return']['int-endorsement-details-iO']['error-lists']['err-description']['#text'];
}
else if(isset($resultData['soapenv:Envelope']['soapenv:Body']['soapenv:Fault']['soapenv:Code']['soapenv:Value']['#text'])) { 
$error ='1## Sorry propero webservice not available right now.Please try later';	
} else {
    $error = '';
    
    if($strCaseNumber!="" && $deployment == 'crm' )
    {
      $queryStr = "CaseNumber=$strCaseNumber";
      $service_url = CRMSERVICEURL."FrontEndEndorsementOnClickModifyRecords?".$queryStr; 
      $res = getXMLResponseServiceCrm($service_url);
    }

    $viewMsg = '';
    
    if ($policyNumber != '') {
		
		// Get last Insert ID from API Response
		$get_last_insert_id = !empty($resultData['soapenv:Envelope']['soapenv:Body']['ns:PolicyEndorsementResponse']['ns:return']['int-endorsement-details-iO']['last_insert_id']['#text']) ? ($resultData['soapenv:Envelope']['soapenv:Body']['ns:PolicyEndorsementResponse']['ns:return']['int-endorsement-details-iO']['last_insert_id']['#text']) : '' ;	
	    
		//For data insert into endorsement_reports table
		$date = date("Y-m-d H:i:s");
		
		$insert_status = ($deployment == 'crm') ? 1 : ($deployment == 'propero') ? 2 : 3;
		// $insert_sql="INSERT INTO `endorsement_reports` SET `policyNumber`='".@$policyNumber."',`endorsmentType`='".@$endorsmentDropDown."',`zreqnum`='".@$strCaseNumber."',`endorsmentDate`='".@$date."',`status`='1', `add_status`='".$insert_status."'";   
		
		//modified query to add extra coloumn for agent id prop-3650
		/* $insert_sql="INSERT INTO `endorsement_reports` SET `policyNumber`='".@$policyNumber."',`old_value`='".@$old_value."',`new_value`='".@$new_value."',product_type ='".@$product_type."' , customerid ='".@$CLNTNUM."' ,dependentid ='".@$dependentId."' ,`endorsmentType`='".@$endorsmentDropDown."',`zreqnum`='".@$strCaseNumber."',`endorsmentDate`='".@$date."',`status`='1', `add_status`='".$insert_status."', `agent_id`='".@$agent_id."'";    
		$stdid = mysqlQuery($insert_sql);
		$last_insert_id = mysqliInsertId(); */
		
		$last_insert_id = $get_last_insert_id;

        $POLICY_NUMBER = $policyNumber;
        $hashCode = hash('sha256', "proposal-" . $POLICY_NUMBER);
        $EmailPDF = "'". $endorsmentDropDown . "','" . $hashCode . "','" . $POLICY_NUMBER . "','" . $strCaseNumber."','" . $last_insert_id."','" . $dependentId."','".$new_value."','".$deployment."','".$CLNTNUM."'";
        $urlVP = "createpdf.php?clientNo=" . $POLICY_NUMBER . "&code=" . $hashCode . "&type=view&zreqnum=" . $strCaseNumber . "&endorsmentType=" . $endorsmentDropDown."&lastId=" . $last_insert_id . "&dependentId=".$dependentId. "&ChangeTo=".$new_value. "&deployment=".$deployment."&customer_id=".$CLNTNUM;
		$urlDownload ="downloadPdf.php?clientNo=" . $POLICY_NUMBER."&date=".$REQDATE;
                
                $pdfEmail ="downloadPdf.php?clientNo=" . $POLICY_NUMBER."&date=".$REQDATE."&vtype=email&lid=".$last_insert_id;
                $pdfView ="downloadPdf.php?clientNo=" . $POLICY_NUMBER."&date=".$REQDATE."&vtype=view";
                
		$viewMsg .= '<table border="2" cellpadding="10"><tr><td colspan ="4"align="center">Endorsement done Successfully!</td></tr><tr><th>Policy no</th><th>Old values</th><th>New Values</th><th>Action</th></tr>';
		
		if(strpos($old_value , ",")!== false || (strpos($old_value , "@$")!== false)){
			$old_val_arr=$new_val_arr=array();
			$old_value_data=$new_value_data='';
			
			if($endorsmentDropDown == '08'){
				$new_val_arr =explode('@$' , $new_value);
				$old_val_arr =explode('@$' , $old_value);
			} else {
				$new_val_arr =explode(',' , $new_value);
				$old_val_arr =explode(',' , $old_value);

			}
			$length=count($old_val_arr);
			for($i=0;$i<$length;$i++){
				$old_value_data .= $old_val_arr[$i]."<br>";
				$label_arr=explode(':',$old_val_arr[$i]);
				$new_value_data .= $label_arr[0].": " . $new_val_arr[$i]."<br>";
			}
			 $old_value = $old_value_data;
			 $new_value = $new_value_data;
		}else{
			$arr=explode(':' , $old_value);
			$old_value =$arr[1]; 
		}
		 if(empty($old_value)){
			 $old_value ="N/A";
		 }
		$urlDownloadtravelcrm ="downloadPdf.php?clientNo=" .$POLICY_NUMBER."&date=".$REQDATE;
                $EmailPDFtravelcrm = "'". $POLICY_NUMBER . "','" . $ZREQNUM . "','" . $deployment . "','" . $last_insert_id."'";
		if($product_type == 'travel' && $deployment=='crm'){
                    $viewMsg .= '<tr><td>'.$POLICY_NUMBER.'</td><td>'.$old_value.'</td><td  style="color:red;">'.$new_value.'</td>
                            <td><a target="_blank" href="'.$urlDownloadtravelcrm.'"><img src="img/view_pdf.jpg"/></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript://" onclick="EmailTravelPDF(' . $EmailPDFtravelcrm . ');">
                            <img src="img/email_pdf.jpg"/></a></td></tr></table>';
                }else if($product_type == 'travel'){	
		 $viewMsg .= '<tr><td>'.$POLICY_NUMBER.'</td><td>'.$old_value.'</td><td style="color:red;">'.$new_value.'</td><td><a target="_blank" href="'.$urlDownload.'" >Download Pdf</a> </td></tr></table>';
		}else{
			$viewMsg .= '<tr><td>'.$POLICY_NUMBER.'</td><td>'.$old_value.'</td><td  style="color:red;">'.$new_value.'</td><td><a onClick="hideModifyDataSetVal();" href="'.$urlVP.'" ><img src="img/view_pdf.jpg"/></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript://" onclick="EmailPDF(' . $EmailPDF . ');"><img src="img/email_pdf.jpg"/></a></td></tr></table>';
		}
    }
    $error = '2##'.$viewMsg;
}

echo $error;
exit;