<?php
define("SITEURL", "https://rhicluat.religarehealthinsurance.com/");
//define("ENDPOINTURL", "https://10.216.9.176:80/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=ReligareHealth,cn=cordys,cn=defaultInst,o=religare.in");
define("ENDPOINTURL", "https://cordysuat.religarehealthinsurance.com/cordys/com.eibus.web.soap.IPGateway.RHIGateway.wcp?organization=o=ReligareHealth,cn=cordys,cn=defaultInst,o=religare.in");
define("WSDLURL", "http://10.216.6.50/cordys/WSDLGateway.wcp?service=http%3A%2F%2Fschemas.cordys.com%2Fdefault/SelfEndorsementBPM_WS&organization=o%3DReligareHealth%2Ccn%3Dcordys%2Ccn%3DdevInst%2Co%3Dreligare.in&version=isv");
//define("CRMSERVICEURL", "http://10.216.30.121:1001/PostCompletionEndorsement.svc/");
//define("CRMSERVICEURL", "http://amicusmscrmuat.religarehealthinsurance.com:1001/PostCompletionEndorsement.svc/");
define("CRMSERVICEURL", "http://amicusmscrmstg.religarehealthinsurance.com/PostCompletionEndorsement.svc/");
define("WSDLPDF", "http://10.216.9.176/cordys/WSDLGateway.wcp?service=http%3A%2F%2Fschemas.cordys.com%2Fdefault/GET_PDFBpmWS&organization=o%3DReligareHealth%2Ccn%3Dcordys%2Ccn%3DOTInst%2Co%3Dreligare.in&version=isv");
//define("PROPEROWSDL" , "http://10.216.9.112:8090/relinterface/services/RelSymbiosysServices?wsdl");
//define("PROPEROENDPOINT" , "http://10.216.9.112:8090/relinterface/services/RelSymbiosysServices.RelSymbiosysServicesHttpSoap12Endpoint/");

define("PROPEROWSDL","https://apiuat.religarehealthinsurance.com/relinterface/services/RelSymbiosysServices?wsdl");
define("PROPEROENDPOINT","https://apiuat.religarehealthinsurance.com/relinterface/services/RelSymbiosysServices.RelSymbiosysServicesHttpSoap12Endpoint/");

define("LOGO_URL", "https://rhicluat.religarehealthinsurance.com/img/religre_logo.jpg");

$endorsmentDropDown = array(
    '01' => "Modification of Policyholder name",
    '15' => "Modification of Insured Name",
    '08' => "Change in Address",
    '12' => "Change in E-mail ID",
    '11' => "Change in Phone No.",
    '20' => "Addition of PAN No.",
    '05' => "Nominee Details",
    '23' => "Rectification in Marital Status",
    '16' => "Mother Maiden Name"
);
$student_edd =array(
	'31' => "University Details",
	'32' => "Course Name",
	'30' => "Sponsor Details"
);
/** Faveo allowed Endorsement Types **/
$endorsmentDropDown_faveo = array(
    '01' => "Modification of Policyholder name",
    '15' => "Modification of Insured Name",
    '08' => "Change in Address",
    '12' => "Change in E-mail ID",
    '11' => "Change in Phone No.",
    '05' => "Nominee Details",
    '23' => "Rectification in Marital Status",
);
/** Faveo allowed Relations**/
$relations_faveo = array(
    'FATH' =>"Father",
    'MOTH' =>"Mother",
    'BOTH' =>"Brother",
    'SIST' =>"Sister",
    'SONM' =>"Son",
    'UDTR' =>"Daughter"
);
/** CRM allowed Relations**/
$relations_crm = array(
    'SIBL' =>'Sibling',
    'WIFE' =>'Wife',
    'FATH' =>'Father',
    'HUSB' =>'Husband',
    'SONM' =>'Son',
    'MOTH' =>'Mother',
    'UDTR' =>'Daughter',
    'GMOT' =>'Grand Mother',
    'GFAT' =>'Grand Father',
    'MMBR' =>'Sister-In-Law',
    'MDTR' =>'Brother-In-Law',
    'BOTH' =>'Brother',
    'SIST' =>'Sister',
    'MLAW' =>'Mother-In-Law'
);
session_start();
if (!isset($_SESSION['token'])) {
    $_SESSION['token']=bin2hex(openssl_random_pseudo_bytes(32));
}
function sanitize_data($input_data) {
    $searchArr = array("document", "write", "alert", "%", "@", "$", ";", "+", "|", "#", "<", ">", ")", "(", "'", "\'", ",");
    $input_data = str_replace("script", "", $input_data);
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

function sanitize_data_email($input_data) {
    $searchArr = array("document", "write", "alert", "%", "$", ";", "+", "|", "#", "<", ">", ")", "(", "'", "\'");
    $input_data = str_replace("script", "", $input_data);
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function sanitize_data_address($input_data) {
    $searchArr = array("document", "write", "alert", "%",";", "+", "#", "<", ">", ")", "(", "'", "\'");
    $input_data = str_replace("script", "", $input_data);
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

function getXMLResponse($data, $endpointurl='') {
	if($endpointurl!=''){
	 $policyURL=$endpointurl;	
	}else{
    $policyURL = ENDPOINTURL;
	}
    $headers = array(
        "Content-Type: text/xml;charset=\"utf-8\"",
        "SOAPAction: \"http://CrmService/WebService/Createlead\"",
        "Content-length: " . strlen($data),
        "Authorization: Basic Y2F0YWJhdGljdXNlcjpjb3J1YXRAMTIz"
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $policyURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $xmlResponse1 = curl_exec($ch);
    $ch_info = curl_getinfo($ch);
    curl_close($ch);
    return $xmlResponse1;
}


function getXMLResponseCrm($data) {
    $policyURL = "http://10.216.5.129:5555/ISV/FrontEndEndorsementService/EndorsementService.asmx";
    $soapaction = "";
    $username = "niranjan.rajput";
    $password = "religare@123";
    $headers = array(
        "POST /ISV/FrontEndEndorsementService/EndorsementService.asmx HTTP/1.1",
        "Host: 10.216.5.129",
        "Content-Type: application/soap+xml; charset=utf-8",
        "Content-length: " . strlen($data)
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $policyURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_NTLM);
    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    $xmlResponse1 = curl_exec($ch);
    $ch_info = curl_getinfo($ch);
    curl_close($ch);

    return $xmlResponse1;
}

function getXMLResponseServiceCrm($srurl)
{   
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $srurl);
    //curl_setopt($ch, CURLOPT_POST, count($queryStr));
    //curl_setopt($ch, CURLOPT_POSTFIELDS, $queryStr);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    $xmlResponse2 = curl_exec($ch);
    $ch_info = curl_getinfo($ch);
    curl_close($ch);
    return $xmlResponse2;
}

$stateArray = array(
    "ANDAMAN" => "ANDAMAN & NICOBAR",
    "JAMMU" => "JAMMU & KASHMIR",
    "DADRA" => "DADRA & NAGAR HAVELI",
    "DAMAN" => "DAMAN & DIU",
);

function checkStateData($stateData, $stateArray) {
    $stateData1 = strtoupper(trim($stateData));
    if (array_key_exists($stateData1, $stateArray)) {
       $resultData = $stateArray[$stateData1];
    } else {
        $resultData = $stateData1;
    }
    return $resultData;
}



?>
