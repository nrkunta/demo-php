<?php
@ob_start();
include_once('api/xml2array.php');
include_once("api/domxml-php4-to-php5.php");
include_once("admin/inc/db_config.php");
include_once("inc/config.php");
include_once('api/api_prop.php');
include_once('function.php');
//CSRF Validation check  
$csrf= sanitize_data(@$_POST['token']);
if(isset($csrf)){
    if($csrf!=$_SESSION["token"]){
        echo "CSRF Validation failed";
    }
}
$show_card = 0;
$oldData = '';
$newData = '';
$deployment     = trim(sanitize_data($_REQUEST['deployment']));
$strCaseNumber  = trim(sanitize_data($_REQUEST['zreqnum']));
$dependentId    = trim(sanitize_data($_REQUEST['dependentId']));
$policyNumber   = trim(sanitize_data($_REQUEST['policyNumber']));
$endorsmentType = trim(sanitize_data($_REQUEST['endorsmentType']));
$keyMod         = $endorsmentDropDown[$endorsmentType];
$rType = 2;
$endorsmentRepostLastId = trim(sanitize_data($_REQUEST['endorsmentRepostLastId']));
$pdf_fileName = $policyNumber . '-' . time();
$customerId = trim(sanitize_data($_REQUEST['customer_id']));
$key_flag = 0;
 // PP-1536 Changes
$ChangeTo     = trim(sanitize_data_email($_REQUEST['ChangeTo']));



if ($endorsmentType == '21') {
    $ChangeTo = trim(sanitize_data($_REQUEST['ChangeTo']));
}
/*
$strCaseNumber='1912112082';
$deployment='crm';
$rType = 1;
*/
if ($strCaseNumber != "" && $rType != "" && $deployment == 'crm') {
    $queryStr = "CaseNumber=$strCaseNumber&Type=$rType";
     $service_url = CRMSERVICEURL . "FrontEndEndorsement?" . $queryStr;
   $resultData = getXMLResponseServiceCrm($service_url);
    file_put_contents("data/crm/view/" . $strCaseNumber . "_Response.xml", $resultData);
    $key_flag = file_get_contents("data/crm/view/" . $strCaseNumber . "_Response.xml", $resultData);
}

if ($deployment != 'crm') {
    $key_flag = 1;
}

/* getting new value */
$xmlData = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:rel="http://relinterface.insurance.symbiosys.c2lbiz.com"
					xmlns:xsd="http://intf.insurance.symbiosys.c2lbiz.com/xsd">
					<soap:Header/>
					<soap:Body>
					  <rel:getPolicyDetails>
						 <rel:intGetPolicyIO>
							<xsd:policyNum>' . $policyNumber . '</xsd:policyNum>
						 </rel:intGetPolicyIO>
					  </rel:getPolicyDetails>
				   </soap:Body>
				</soap:Envelope>';
file_put_contents("data/renewal/email_" . $policyNumber . '-' . $endorsmentType . "_Request.xml", $xmlData);
$resultDataNew = soapReq($xmlData, 'getPolicyDetails');
file_put_contents("data/renewal/email_" . $policyNumber . '-' . $endorsmentType . "_Response.xml", $resultDataNew);
$xmlNew = new xml2array($resultDataNew);
$dataArrNew = $xmlNew->getResult();
$dataNew = @$dataArrNew['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['policy'];



//getting old value from renewal response
$resultData = file_get_contents("data/renewal/" . $policyNumber . "-" . $endorsmentType . "_Response.xml");
$xml = new xml2array($resultData);
$dataArr = $xml->getResult();
$data = @$dataArr['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['policy'];


/* Old values */

//$customerId= $data['list-party-dOList']['customer-id']['#text'];
$member_id = $data['list-policy-product-dOList']['list-policy-product-insured-dOList']['member-id']['#text'];
$firstName1 = @$data['list-party-dOList']['first-name1']['#text'];
$lastName1 = @$data['list-party-dOList']['last-name1']['#text'];
$titleCd = @$data['list-party-dOList']['title-cd']['#text'];
$emilData = @$data['list-party-dOList']['list-party-email-dOList']['email-address']['#text'];
$panCardCd = @$data['list-party-dOList']['party-identity-dOs']['identity-num']['#text'];
$contactNum = @$data['list-party-dOList']['list-party-contact-dOList']['contact-num']['#text'];
$addressLine1Lang1 = @$data['list-party-dOList']['list-party-address-dOList']['address-line1-lang1']['#text'];
$addressLine2Lang1 = @$data['list-party-dOList']['list-party-address-dOList']['address-line2-lang1']['#text'];
$stateCd = @$data['list-party-dOList']['list-party-address-dOList']['state-cd']['#text'];
$cityCd = @$data['list-party-dOList']['list-party-address-dOList']['city-cd']['#text'];
$areaCd = @$data['list-party-dOList']['list-party-address-dOList']['area-cd']['#text'];
$pinCode = @$data['list-party-dOList']['list-party-address-dOList']['pin-code']['#text'];
$maiden_name = @$data['list-party-dOList']['maiden-name']['#text'];
$course_detail = $data['list-policy-additional-fields-dOList']['course-details']['#text'];
$university_name = @$data['list-policy-additional-fields-dOList']['field8']['#text'];
$university_address = @$data['list-policy-additional-fields-dOList']['university-address']['#text'];
$sponsor_name = @$data['list-policy-additional-fields-dOList']['sponsor-name']['#text'];
$sponsor_dob = get_timeformat(@$data['list-policy-additional-fields-dOList']['sponsor-dOB']['#text'], 'sponsor');
$sponsor_rel = @$data['list-policy-additional-fields-dOList']['relationship-to-student']['#text'];
$martial_status = @$data['list-party-dOList']['marital-status-cd']['#text'];
$nominee_title = @$data['list-policy-additional-fields-dOList']['field9']['#text'];
$nominee_first_name = @$data['list-policy-additional-fields-dOList']['field10']['#text'];
$nominee_last_name = @$data['list-policy-additional-fields-dOList']['field11']['#text'];
$nominee_dob = isset($data['list-policy-additional-fields-dOList']['field17']['#text']) ? date('d/m/Y', strtotime(@$data['list-policy-additional-fields-dOList']['field17']['#text'])) : '';
$nominee_relation = @$data['list-policy-additional-fields-dOList']['field12']['#text'];



// new values
 // PP-1536 Changes
$ChangeTo_array=explode("@",$ChangeTo);
//$contactNumNew=$contactNum;
$contactNumNew = @$dataNew['list-party-dOList']['list-party-contact-dOList']['contact-num']['#text'];
$addressLine1Lang1New=@$dataNew['list-party-dOList']['list-party-address-dOList'][0]['address-line1-lang1']['#text'];
$addressLine2Lang1New = @$dataNew['list-party-dOList']['list-party-address-dOList'][0]['address-line2-lang1']['#text'];
$stateCdNew = @$dataNew['list-party-dOList']['list-party-address-dOList'][0]['state-cd']['#text'];
$cityCdNew = @$dataNew['list-party-dOList']['list-party-address-dOList'][0]['city-cd']['#text'];
$areaCdNew = @$dataNew['list-party-dOList']['list-party-address-dOList'][0]['area-cd']['#text'];
//$addressLine1Lang1New=$ChangeTo_array[0];
$pinCodeNew=@$dataNew['list-party-dOList']['list-party-address-dOList'][0]['pin-code']['#text'];
//$pinCodeNew=$ChangeTo_array[5];
/*
$addressLine2Lang1New=$ChangeTo_array[1];
$areaCdNew=$ChangeTo_array[2];
$cityCdNew=$ChangeTo_array[3];
$stateCdNew=$ChangeTo_array[4];
$pinCodeNew=$ChangeTo_array[5];
  */
/* if the value is n/a  , empty value is showing in pdf as a part of address change issue -prop3650  */
if ($areaCdNew == 'N/A') {
    $areaCdNew = "";
}

if ($areaCd == 'N/A') {
    $areaCd = "";
}

//$pinCodeNew = @$dataNew['list-party-dOList']['list-party-address-dOList']['pin-code']['#text'];
$productFamily = @$dataNew['base-product-family-cd']['#text'];
$nominee_new_title = @$dataNew['list-policy-additional-fields-dOList']['field9']['#text'];
$nominee_new_first_name = @$dataNew['list-policy-additional-fields-dOList']['field10']['#text'];
$nominee_new_last_name = @$dataNew['list-policy-additional-fields-dOList']['field11']['#text'];
$nominee_new_dob = !empty($dataNew['list-policy-additional-fields-dOList']['field17']['#text']) ? get_timeformat(@$dataNew['list-policy-additional-fields-dOList']['field17']['#text'], 'nominee') : '';
//$nominee_new_dob = isset($dataNew['list-policy-additional-fields-dOList']['field17']['#text']) ? date('d/m/Y', strtotime(@$dataNew['list-policy-additional-fields-dOList']['field17']['#text'])) : '';
$nominee_new_relation = @$dataNew['list-policy-additional-fields-dOList']['field12']['#text'];
$newemilData = @$dataNew['list-party-dOList']['list-party-email-dOList']['email-address']['#text'];

//**********************Dates***********************
$endorsementDate = date("d M Y");
$startDate = get_timeformat(@$dataNew['policy-commencement-dt']['#text'], 'commencement');
if (strtotime($endorsementDate) > strtotime($startDate)) {
    $effDate = $endorsementDate;
} else {
    $effDate = date("d M Y",strtotime($startDate));
}

//*****************************************************

$viewTime = date("Y-m-d H:i:s");





switch ($endorsmentType) {
    case '01': {
            // generate pdf data if proposer = self insured 
            if (($dataNew['list-party-dOList']['relation-cd']['#text'] == 'SELF') && ($dataNew['list-party-dOList']['role-cd']['#text'] == 'PROPOSER')) {
                $dob = get_timeformat($dataNew['list-party-dOList']['birth-dt']['#text'], 'pdf');
                $pdf_name_data = $dataNew['list-party-dOList']['first-name1']['#text'] . "@@@" . $dataNew['list-party-dOList']['last-name1']['#text'] . "@@@" . $customerId . "@@@" . $dob;
                $show_card = 1;
            }
            $oldData = ucfirst(strtolower($titleCd)) . ' ' . $firstName1 . ' ' . $lastName1;
            $newData = ucfirst(@$dataNew['list-party-dOList']['title-cd']['#text']) . ' ' . @$dataNew['list-party-dOList']['first-name1']['#text'] . ' ' . @$dataNew['list-party-dOList']['last-name1']['#text'];
            break;
        }
    case '15': {
            $show_card = 1;
            if ($dependentId == '01') {
                if ($dataNew['list-party-dOList'][0]['last-name1']['#text'] == 'N/A') {
                    $dataNew['list-party-dOList'][0]['last-name1']['#text'] = " ";
                }
                if ($data['list-party-dOList'][0]['last-name1']['#text'] == 'N/A') {
                    $data['list-party-dOList'][0]['last-name1']['#text'] = " ";
                }

                $dob = get_timeformat($dataNew['list-party-dOList'][0]['birth-dt']['#text'], 'pdf');
                $oldData = ucfirst(strtolower(@$data['list-party-dOList'][0]['title-cd']['#text'])) . ' ' . @$data['list-party-dOList'][0]['first-name1']['#text'] . ' ' . @$data['list-party-dOList'][0]['last-name1']['#text'];
                $newData = ucfirst(strtolower(@$dataNew['list-party-dOList'][0]['title-cd']['#text'])) . ' ' . @$dataNew['list-party-dOList'][0]['first-name1']['#text'] . ' ' . @$dataNew['list-party-dOList'][0]['last-name1']['#text'];
                $pdf_name_data = $dataNew['list-party-dOList'][0]['first-name1']['#text'] . "@@@" . $dataNew['list-party-dOList'][0]['last-name1']['#text'] . "@@@" . $customerId . "@@@" . $dob;
            } else if ($dependentId == '02') {
                if ($dataNew['list-party-dOList'][1]['last-name1']['#text'] == 'N/A') {
                    $dataNew['list-party-dOList'][1]['last-name1']['#text'] = " ";
                }

                if ($data['list-party-dOList'][1]['last-name1']['#text'] == 'N/A') {
                    $data['list-party-dOList'][1]['last-name1']['#text'] = " ";
                }

                $dob = get_timeformat($dataNew['list-party-dOList'][1]['birth-dt']['#text'], 'pdf');
                $oldData = ucfirst(strtolower(@$data['list-party-dOList'][1]['title-cd']['#text'])) . ' ' . @$data['list-party-dOList'][1]['first-name1']['#text'] . ' ' . @$data['list-party-dOList'][1]['last-name1']['#text'];
                $newData = ucfirst(strtolower(@$dataNew['list-party-dOList'][1]['title-cd']['#text'])) . ' ' . @$dataNew['list-party-dOList'][1]['first-name1']['#text'] . ' ' . @$dataNew['list-party-dOList'][1]['last-name1']['#text'];
                $pdf_name_data = $dataNew['list-party-dOList'][1]['first-name1']['#text'] . "@@@" . $dataNew['list-party-dOList'][1]['last-name1']['#text'] . "@@@" . $customerId . "@@@" . $dob;
            } else if ($dependentId == '03') {
                if ($dataNew['list-party-dOList'][2]['last-name1']['#text'] == 'N/A') {
                    $dataNew['list-party-dOList'][2]['last-name1']['#text'] = " ";
                }

                if ($data['list-party-dOList'][2]['last-name1']['#text'] == 'N/A') {
                    $data['list-party-dOList'][2]['last-name1']['#text'] = " ";
                }

                $dob = get_timeformat($dataNew['list-party-dOList'][2]['birth-dt']['#text'], 'pdf');
                $oldData = ucfirst(strtolower(@$data['list-party-dOList'][2]['title-cd']['#text'])) . ' ' . @$data['list-party-dOList'][2]['first-name1']['#text'] . ' ' . @$data['list-party-dOList'][2]['last-name1']['#text'];
                $newData = ucfirst(strtolower(@$dataNew['list-party-dOList'][2]['title-cd']['#text'])) . ' ' . @$dataNew['list-party-dOList'][2]['first-name1']['#text'] . ' ' . @$dataNew['list-party-dOList'][2]['last-name1']['#text'];
                $pdf_name_data = $dataNew['list-party-dOList'][2]['first-name1']['#text'] . "@@@" . $dataNew['list-party-dOList'][2]['last-name1']['#text'] . "@@@" . $customerId . "@@@" . $dob;
            } else if ($dependentId == '04') {
                if ($dataNew['list-party-dOList'][3]['last-name1']['#text'] == 'N/A') {
                    $dataNew['list-party-dOList'][3]['last-name1']['#text'] = " ";
                }

                if ($data['list-party-dOList'][3]['last-name1']['#text'] == 'N/A') {
                    $data['list-party-dOList'][3]['last-name1']['#text'] = " ";
                }

                $dob = get_timeformat($dataNew['list-party-dOList'][3]['birth-dt']['#text'], 'pdf');
                $oldData = ucfirst(strtolower(@$data['list-party-dOList'][3]['title-cd']['#text'])) . ' ' . @$data['list-party-dOList'][3]['first-name1']['#text'] . ' ' . @$data['list-party-dOList'][3]['last-name1']['#text'];
                $newData = ucfirst(strtolower(@$dataNew['list-party-dOList'][3]['title-cd']['#text'])) . ' ' . @$dataNew['list-party-dOList'][3]['first-name1']['#text'] . ' ' . @$dataNew['list-party-dOList'][3]['last-name1']['#text'];
                $pdf_name_data = $dataNew['list-party-dOList'][3]['first-name1']['#text'] . "@@@" . $dataNew['list-party-dOList'][3]['last-name1']['#text'] . "@@@" . $customerId . "@@@" . $dob;
            } else if ($dependentId == '05') {
                if ($dataNew['list-party-dOList'][4]['last-name1']['#text'] == 'N/A') {
                    $dataNew['list-party-dOList'][4]['last-name1']['#text'] = " ";
                }

                if ($data['list-party-dOList'][4]['last-name1']['#text'] == 'N/A') {
                    $data['list-party-dOList'][4]['last-name1']['#text'] = " ";
                }

                $dob = get_timeformat($dataNew['list-party-dOList'][4]['birth-dt']['#text'], 'pdf');
                $oldData = ucfirst(strtolower(@$data['list-party-dOList'][4]['title-cd']['#text'])) . ' ' . @$data['list-party-dOList'][4]['first-name1']['#text'] . ' ' . @$data['list-party-dOList'][4]['last-name1']['#text'];
                $newData = ucfirst(strtolower(@$dataNew['list-party-dOList'][4]['title-cd']['#text'])) . ' ' . @$dataNew['list-party-dOList'][4]['first-name1']['#text'] . ' ' . @$dataNew['list-party-dOList'][4]['last-name1']['#text'];
                $pdf_name_data = $dataNew['list-party-dOList'][4]['first-name1']['#text'] . "@@@" . $dataNew['list-party-dOList'][4]['last-name1']['#text'] . "@@@" . $customerId . "@@@" . $dob;
            }
            break;
        }
    case '08': {
            $addressLine1Lang1 = @$data['list-party-dOList']['list-party-address-dOList'][0]['address-line1-lang1']['#text'];
            $addressLine2Lang1 = @$data['list-party-dOList']['list-party-address-dOList'][0]['address-line2-lang1']['#text'];
            $stateCd = @$data['list-party-dOList']['list-party-address-dOList'][0]['state-cd']['#text'];
            $cityCd = @$data['list-party-dOList']['list-party-address-dOList'][0]['city-cd']['#text'];
            $areaCd = @$data['list-party-dOList']['list-party-address-dOList'][0]['area-cd']['#text'];
            $pinCode = @$data['list-party-dOList']['list-party-address-dOList'][0]['pin-code']['#text'];
            $oldData = $addressLine1Lang1 . ' ' . $addressLine2Lang1 . ',' . $areaCd . ', ' . $cityCd . ', ' . $stateCd . ', ' . $pinCode;
           
        //    $newData = @$dataNew['list-party-dOList']['list-party-address-dOList']['address-line1-lang1']['#text'] . ' ' . @$dataNew['list-party-dOList']['list-party-address-dOList']['address-line2-lang1']['#text'] . ',' . @$areaCdNew . ', ' . @$dataNew['list-party-dOList']['list-party-address-dOList']['city-cd']['#text'] . ', ' . @$dataNew['list-party-dOList']['list-party-address-dOList']['state-cd']['#text'] . ', ' . @$dataNew['list-party-dOList']['list-party-address-dOList']['pin-code']['#text'];
             // PP-1536 Changes
            $newData=$addressLine1Lang1New.', '.$addressLine2Lang1New.', '.$areaCdNew.', '.$cityCdNew.', '.$stateCdNew.' - '.$pinCodeNew;
         
        //  $newData = preg_replace("/,+/", ",", $newData);
            break;
        }
    case '12': {
            if($emilData==''){
                $emilData='N/A';
            }
            $oldData = $emilData;
            $newData = @$dataNew['list-party-dOList']['list-party-email-dOList']['email-address']['#text'];
            break;
        }
    case '11': {
            if($contactNum==''){
                $contactNum='N/A';
            }
            $oldData = $contactNum;
            $newData = @$dataNew['list-party-dOList']['list-party-contact-dOList']['contact-num']['#text'];
            break;
        }
    case '20': {
            if($panCardCd==''){
                $panCardCd='N/A';
            }
            $oldData = $panCardCd;
            $newData = @$dataNew['list-party-dOList']['party-identity-dOs']['identity-num']['#text'];
            break;
        }

    case '16': {
            $oldData = $maiden_name;
            $newData = @$dataNew['list-party-dOList']['maiden-name']['#text'];
            break;
        }

    case '29': {
            $oldData = $martial_status;
            $newData = $dataNew['list-party-dOList']['marital-status-cd']['#text'];
            break;
        }

    case '32': {

            $oldData = $course_detail;
            $newData = @$dataNew['list-policy-additional-fields-dOList']['course-details']['#text'];
            break;
        }
    case '31': {

            $oldData = $university_name . "," . $university_address;
            $newData = @$dataNew['list-policy-additional-fields-dOList']['field8']['#text'] . "," . $dataNew['list-policy-additional-fields-dOList']['university-address']['#text'];
            break;
        }
    case '30': {

            $oldData = $sponsor_name . "," . $sponsor_dob . "," . $sponsor_rel;
            $newData = @$dataNew['list-policy-additional-fields-dOList']['sponsor-name']['#text'] . ", " . get_timeformat(@$dataNew['list-policy-additional-fields-dOList']['sponsor-dOB']['#text'], 'sponsor') . ", " . @$dataNew['list-policy-additional-fields-dOList']['relationship-to-student']['#text'];
            break;
        }
    case '05': {
            $oldData = $nominee_title . " " . $nominee_first_name . " " . $nominee_last_name . " " . $nominee_dob . "," . $nominee_relation;
            $newData = $nominee_new_title . " " . $nominee_new_first_name . " " . $nominee_new_last_name . "," . $nominee_new_dob . "," . $nominee_new_relation;
            $newData = preg_replace("/,+/", ",", $newData);
            break;
        }
}

//$firstLastName = ucfirst(strtolower(@$dataNew['partyDO']['titleCd']['#text'])) . ' ' . @$dataNew['partyDO']['firstName1']['#text'] . ' ' . @$dataNew['partyDO']['lastName1']['#text'];
$firstLastName = ucfirst(strtolower(@$dataNew['list-party-dOList']['title-cd']['#text'])) . ' ' . @$dataNew['list-party-dOList']['first-name1']['#text'] . ' ' . @$dataNew['list-party-dOList']['last-name1']['#text'];

if (empty($newemilData) || $newemilData == '') {
    echo "Email ID not Registered ";
    exit;
}



if ($customerId != '') {
    include_once('pdf.php');
    include_once 'pdftohtml.php';
    $file_name = $pdf_fileName . '.pdf';
//For update pdf file in endorsement_reports table

    if ($endorsmentRepostLastId != '') {
        $sql_query = "UPDATE `endorsement_reports` SET `uploadFile`='" . @$file_name . "',`keyFlag`='" . @$key_flag . "',`emailId`='" . @$newemilData . "',`viewDate`='" . @$viewTime . "' WHERE `id`='" . @$endorsmentRepostLastId . "'"; //function to update records
        $stdid1 = mysqlQuery($sql_query);
    }

    ob_clean();
    $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->Output("data/" . $file_name, 'F');
}
$bodytextM = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Care Health Insurance</title>
</head>
<body style="margin:0px; padding:0px;">

<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #919191;">
<tr>
  <td>
  	<table width="550" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
      <td height="100" align="top" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;"><img src="' . LOGO_URL . '" border="0" /></td>  
  </tr>
  <tr>
    <td height="30" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;">Dear Sir/Madam,</td>
  </tr>
  <tr>
    <td height="30" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;">Greetings from Care Health Insurance!</td>
  </tr>
  <tr>
    <td height="30" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;">Please find the attached requested pdf.</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="30" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;"><p>Wishing You Health Hamesha...! </p></td>
  </tr>
  <tr>
    <td height="30" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;"><p>Team Care Health Insurance </p></td>
  </tr>
  <tr>
    <td height="30" style="font:bold 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;"><p>Note: This is an auto  generatedmail, please do not Reply to this mail. </p></td>
  </tr>

<tr>
	<td height="60">&nbsp;</td>
</tr>
</table>
</body>
</html>';
$subject = "Endorsement Certificate";
//$to = trim($emilData);
$to = trim($newemilData);
$random_hash = md5(date('r', time()));
$file_to_attach = $_SERVER["DOCUMENT_ROOT"] . 'data/' . $file_name;
$headers = "From: chil.portaladmin@careinsurance.com \r\nReply-To: chil.portaladmin@careinsurance.com ";
$headers .= "\r\nContent-Type: multipart/mixed; boundary=\"PHP-mixed-" . $random_hash . "\"";
$attachment = chunk_split(base64_encode(file_get_contents($file_to_attach))); // Set your file path here
$message = "--PHP-mixed-$random_hash\r\n" . "Content-Type: multipart/alternative; boundary=\"PHP-alt-$random_hash\"\r\n\r\n";
$message .= "--PHP-alt-$random_hash\r\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\r\n" . "Content-Transfer-Encoding: 7bit\r\n\r\n";
$message .= $bodytextM;
$message .= "\r\n\r\n--PHP-alt-$random_hash--\r\n\r\n";
$message .= "--PHP-mixed-$random_hash\r\n" . "Content-Type: application/pdf; name=\"$file_name\"\r\n" . "Content-Transfer-Encoding: base64\r\n" . "Content-Disposition: attachment\r\n\r\n";
$message .= $attachment;
$message .= "/r/n--PHP-mixed-$random_hash--";
$mail = mail($to, $subject, $message, $headers);



if ($mail) {
    echo "An email with policy PDF attachement has been sent to your registered email id.";
    unlink("data/" . $file_name);
    exit;
} else {
    unlink("data/" . $file_name);
    echo "Error in sending mail";
}

?>