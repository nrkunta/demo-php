<?php
//download pdf
include_once('api/xml2array.php');
include_once("api/domxml-php4-to-php5.php");
include_once("admin/inc/db_config.php");
include_once("inc/config.php");
include_once('api/api_prop.php');
include_once('pdf.php');
include_once('function.php');
$policyNo = trim(sanitize_data($_REQUEST['clientNo']));
$date = sanitize_data($_REQUEST['date']);
if($date==''){
    $date=time();
}
$xmlReq='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:def="http://schemas.cordys.com/default">
   <soapenv:Header/>
   <soapenv:Body>
      <def:GET_PDFBpmWS>
          <def:policyNo>'.$policyNo.'</def:policyNo>
         <def:ltype>POLSCHD</def:ltype>
      </def:GET_PDFBpmWS>
   </soapenv:Body>
</soapenv:Envelope>';

$resultData = getXMLResponse($xmlReq);
//logs data capturing
file_put_contents("data/policypdf/".$policyNo."_Policypdf_request.xml",$xmlReq);
file_put_contents("data/policypdf/".$policyNo."_Policypdf_response.xml",$resultData);
$xml = new xml2array($resultData);
$dataArr = $xml->getResult();

$data=$dataArr['soapenv:Envelope']['soapenv:Body']['GET_PDFBpmWSResponse']['ns2:GET_PDFResponse']['return']['#text'];

if(strpos($data , "<StreamData>") !== false){
	$dataResult = explode("<StreamData>" , $data);
	$result = trim(str_replace("</StreamData>", " " ,$dataResult[1]));
	header('Content-type: application/pdf');
	header('Content-Disposition: attachment; filename="'.$policyNo.'_'.$date.'.pdf"');
    echo base64_decode($result);
}else{
    echo 'Unable to generate pdf. Please Try Again.';
}
?>