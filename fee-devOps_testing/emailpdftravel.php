<?php
@ob_start();
include_once('api/xml2array.php');
include_once("api/domxml-php4-to-php5.php");
include_once("admin/inc/db_config.php");
include_once("inc/config.php");
include_once('api/api_prop.php');
include_once('function.php');
//CSRF Validation check  
$csrf= sanitize_data(@$_POST['token']);
if(isset($csrf)){
    if($csrf!=$_SESSION["token"]){
        echo "CSRF Validation failed";
    }
}
$rType  = 2;
$strCaseNumber = trim(sanitize_data($_REQUEST['zreqnum']));
$deployment    = trim(sanitize_data($_REQUEST['deployment']));
$endorsmentRepostLastId   = trim(sanitize_data($_REQUEST['endorsmentRepostLastId']));
$policyNo = trim(sanitize_data($_REQUEST['policyNumber']));
$time=time();
$key_flag = 1;
if($strCaseNumber!="" && $rType!="" && $deployment == 'crm')
{
  $queryStr = "CaseNumber=$strCaseNumber&Type=$rType";
  $service_url = CRMSERVICEURL."FrontEndEndorsement?".$queryStr; 
  $resultData = getXMLResponseServiceCrm($service_url);
  file_put_contents("data/crm/view/" . $strCaseNumber . "_Response.xml", $resultData);
  $key_flag = file_get_contents("data/crm/view/" . $strCaseNumber . "_Response.xml", $resultData);
}    
/* getting new value*/
$xmlData = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:rel="http://relinterface.insurance.symbiosys.c2lbiz.com"
					xmlns:xsd="http://intf.insurance.symbiosys.c2lbiz.com/xsd">
					<soap:Header/>
					<soap:Body>
					  <rel:getPolicyDetails>
						 <rel:intGetPolicyIO>
							<xsd:policyNum>'.$policyNo.'</xsd:policyNum>
						 </rel:intGetPolicyIO>
					  </rel:getPolicyDetails>
				   </soap:Body>
				</soap:Envelope>';	
			
$resultDataNew = soapReq($xmlData ,'getPolicyDetails');;
$xmlNew = new xml2array($resultDataNew);
$dataArrNew = $xmlNew->getResult();
$dataNew = @$dataArrNew['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['policy'];
$newemilData=@$dataNew['list-party-dOList']['list-party-email-dOList']['email-address']['#text'];
$viewTime = date("Y-m-d H:i:s");

if(empty($newemilData) || $newemilData == ''){
    echo "Email ID not Registered ";
    exit;
}
//Download pdf into data folder
include_once 'downloadPdfTravel.php';
$pdf_fileName = $policyNo.'-'.$time;
$file_name = $pdf_fileName.'.pdf';
$dir_to_save = "data/";
//Getting the PDF from data folder
if(!file_exists($dir_to_save.$file_name))
{
   echo "Unable to create pdf."; 
   exit;
}
//For update pdf file in endorsement_reports table
if($endorsmentRepostLastId!='')
{
    $sql_query="UPDATE `endorsement_reports` SET `uploadFile`='".@$file_name."',`keyFlag`='".@$key_flag."',`emailId`='".@$newemilData."',`viewDate`='".@$viewTime."' WHERE `id`='".@$endorsmentRepostLastId."'"; //function to update records
    $stdid1 = mysqlQuery($sql_query);
}
$bodytextM = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Care Health Insurance</title>
</head>
<body style="margin:0px; padding:0px;">

<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #919191;">
<tr>
  <td>
  	<table width="550" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td height="100" align="top" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;"><img src="'.LOGO_URL.'" border="0" /></td>
  </tr>
  <tr>
    <td height="30" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;">Dear Sir/Madam,</td>
  </tr>
  <tr>
    <td height="30" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;">Greetings from Care Health Insurance!</td>
  </tr>
  <tr>
    <td height="30" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;">Please find the attached requested pdf.</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="30" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;"><p>Wishing You Health Hamesha...! </p></td>
  </tr>
  <tr>
    <td height="30" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;"><p>Team Care Health Insurance </p></td>
  </tr>
  <tr>
    <td height="30" style="font:bold 12px Verdana, Arial, Helvetica, sans-serif; color:#232323;"><p>Note: This is an auto  generatedmail, please do not Reply to this mail. </p></td>
  </tr>

<tr>
	<td height="60">&nbsp;</td>
</tr>
</table>
</body>
</html>';
$subject = "Endorsement Certificate";
$to = trim($newemilData);
$random_hash = md5(date('r', time()));
$file_to_attach = $_SERVER["DOCUMENT_ROOT"] . 'endorsement/data/' . $file_name;
$headers = "From: rhiclportaladmin@careinsurance.com \r\nReply-To: rhiclportaladmin@careinsurance.com ";
$headers .= "\r\nContent-Type: multipart/mixed; boundary=\"PHP-mixed-" . $random_hash . "\"";
$attachment = chunk_split(base64_encode(file_get_contents($file_to_attach))); // Set your file path here
$message = "--PHP-mixed-$random_hash\r\n" . "Content-Type: multipart/alternative; boundary=\"PHP-alt-$random_hash\"\r\n\r\n";
$message .= "--PHP-alt-$random_hash\r\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\r\n" . "Content-Transfer-Encoding: 7bit\r\n\r\n";
$message .= $bodytextM;
$message .="\r\n\r\n--PHP-alt-$random_hash--\r\n\r\n";
$message .= "--PHP-mixed-$random_hash\r\n" . "Content-Type: application/pdf; name=\"$file_name\"\r\n" . "Content-Transfer-Encoding: base64\r\n" . "Content-Disposition: attachment\r\n\r\n";
$message .= $attachment;
$message .= "/r/n--PHP-mixed-$random_hash--";
$mail=mail($to, $subject, $message, $headers);
ob_end_clean();
if($mail){
echo "An email with policy PDF attachement has been sent to your registered email id.";
unlink($dir_to_save.$file_name);
exit;
}
else{
    unlink($dir_to_save.$file_name);
    echo "Error in sending mail";
}

?>