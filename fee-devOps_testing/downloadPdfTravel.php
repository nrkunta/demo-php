<?php
//download pdf
include_once('api/xml2array.php');
include_once("api/domxml-php4-to-php5.php");
include_once("inc/config.php");
include_once('api/api_prop.php');
include_once('pdf.php');
include_once('function.php');
$xmlReq='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:def="http://schemas.cordys.com/default">
   <soapenv:Header/>
   <soapenv:Body>
      <def:GET_PDFBpmWS>
          <def:policyNo>'.$policyNo.'</def:policyNo>
         <def:ltype>POLSCHD</def:ltype>
      </def:GET_PDFBpmWS>
   </soapenv:Body>
</soapenv:Envelope>';

$resultData = getXMLResponse($xmlReq);
//logs data capturing
file_put_contents("data/policypdf/".$policyNo."_Policypdf_request.xml",$xmlReq);
file_put_contents("data/policypdf/".$policyNo."_Policypdf_response.xml",$resultData);
$xml = new xml2array($resultData);
$dataArr = $xml->getResult();

$data=$dataArr['soapenv:Envelope']['soapenv:Body']['GET_PDFBpmWSResponse']['ns2:GET_PDFResponse']['return']['#text'];

if(strpos($data , "<StreamData>") !== false){
	$dataResult = explode("<StreamData>" , $data);
	$result = trim(str_replace("</StreamData>", " " ,$dataResult[1]));
        $pdf_data= base64_decode($result);
        $file=$policyNo.'-'.$time;
        $dir_to_save = "data/";//Saving pdf in data folder
        file_put_contents($dir_to_save.$file.'.pdf',$pdf_data);
}

?>