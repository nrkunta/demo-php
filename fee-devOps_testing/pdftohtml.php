<?php
if($show_card == 0){
	$disp='none';
	$without_card =1;
}else{
	$disp='';
	$pdf_data_arr =explode("@@@" ,$pdf_name_data);
	$without_card =0;
}
if($endorsmentType=='15' || $endorsmentType=='01')
{
  $user_name = $newData;

}
else 
{ 
  $user_name = ucfirst(strtolower($titleCd)) . ' ' . $firstName1 . ' ' . $lastName1;
}
$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Care Health Insurance</title>
        </head>
        <body>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td width="100%" height="50" align="right" valign="top" style="font:normal 13px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;">
								    <table width="100%" border="0" cellspacing="0" cellpadding="0">
									  <tr>
										<td width="50%" align="left" valign="middle"></td>
										<td width="25%" align="left" valign="middle"></td>
										<td width="25%" align="right" valign="middle"><img src="pdf/logo.jpg" border="0"></td>
									  </tr>
                                    </table>
								</td>
                            </tr>
                            <tr>
                                <td height="30" align="left" valign="top" style="font:normal 9px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="9pt;" face="Gill Sans MT Pro Heavy"><u>Date: '. $currentDate.'</u></font></td>
                            </tr>
                            <tr>
                                <td height="75" valign="top" align="left" style="font:normal 9px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="9pt;" face="Gill Sans MT Pro Heavy">'.$firstLastName.'<br />'.$addressLine1Lang1New; ?>
<?php
                                    if ($addressLine2Lang1New != '') {
                                     $html .= '<br /> '.$addressLine2Lang1New;
                                    }
                                    $html .='<br />'.$areaCdNew.','.$cityCdNew.','.$stateCdNew.', -'.$pinCodeNew.'<br />
                                    Contact No - '.$contactNumNew.'</font></td>
                            </tr>

                            <tr>
                                <td height="9" valign="top" style="font:normal 10px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="9pt;" face="Gill Sans MT Pro Heavy"><b><u>Sub: Endorsement Certificate- '.$policyNumber.'</u></b></font></td>
                            </tr>
                            <tr>
                                <td height="25" valign="top" style="font:normal 9px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="9pt;" face="Gill Sans MT Pro Heavy">Dear Sir/Madam,</font></td>
                            </tr>
                            <tr>
                                <td height="25" valign="top" style="font:normal 9px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="9pt;" face="Gill Sans MT Pro Heavy">Greetings from Care Health Insurance!</font></td>
                            </tr>
                            <tr>
                                <td width="100%" valign="top" style="font:normal 9px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="9pt;" face="Gill Sans MT Pro Heavy">This is with reference to your Request Id '. $strCaseNumber .' received by us on ' . $currentDate .' for effecting changes in your '. ucfirst(strtolower($productFamily)) .' Insurance
                                    Policy '. $policyNumber .' <br />
                                    <br />
                                    We would like to confirm that the following changes have been made in the captioned Policy:</font></td>
									 <br />
                            </tr>
							 <br />
                            <tr>
                                <td width="100%" style="font:normal 8px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;">
								<table width="100%" border="1" cellpadding="10" cellspacing="0" bgcolor="#c0c0c0">
                                        <tr bgcolor="#c0c0c0">
                                            <td width="20%" align="center" valign="top" bgcolor="#FFFFFF" style="font:bold 8px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="8pt;" face="Gill Sans MT Pro Heavy"><b>Proposer /Insured Name</b></font></td>
                                            <td width="20%" align="center" valign="top" bgcolor="#FFFFFF" style="font:bold 8px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="8pt;" face="Gill Sans MT Pro Heavy"><b>Details to be Modified</b></font></td>';
											if($endorsmentType!='21'){ 
                                            $html .= '<td width="30%" align="center" valign="top" bgcolor="#FFFFFF" style="font:bold 8px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="8pt;" face="Gill Sans MT Pro Heavy"><b>Details as appearing in<br />
                                            Original Policy</b></font></td>';
											}
                                           $html .=' <td width="30%" align="center" valign="top" bgcolor="#FFFFFF" style="font:bold 8px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><b><font size="8pt;" face="Gill Sans MT Pro Heavy">Modified Details</font></b></td>
                                        </tr>
                                        <tr bgcolor="#c0c0c0">
                                            <td align="center" valign="top" bgcolor="#FFFFFF" style="font:normal 8px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="8pt;" face="Gill Sans MT Pro Heavy">'. $user_name .'</font></td>
                                            <td align="center" valign="top" bgcolor="#FFFFFF" style="font:normal 8px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="8pt;" face="Gill Sans MT Pro Heavy">'. $keyMod.'</font></td>';
											if($endorsmentType!='21'){
                                             $html .=' <td align="center" valign="top" bgcolor="#FFFFFF" style="font:normal 8px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="8pt;" face="Gill Sans MT Pro Heavy">'. $oldData.'</font></td>';
										   }
                                            $html .= '<td align="center" valign="top" bgcolor="#FFFFFF" style="font:normal 8px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;"><font size="8pt;" face="Gill Sans MT Pro Heavy">'. $newData .'</font></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                               <td width="100%" height="100" valign="top" style="font:normal 15px Gill Sans MT Pro Light,  Verdana, Arial, Helvetica, sans-serif; color:#232323;">
							<font size="9pt;" face="Gill Sans MT Pro Heavy"><br>The effective date of endorsement is '. $effDate.'. All other terms and conditions of the Policy remain
unchanged, unaltered and binding on you and the Insured.<br><br />
                                    In case of any discrepancy with the above stated modifications, you can get in touch with us at the below mentioned coordinates, within 15
days from the effective date of change or else it will be construed as acceptable and correct.
                                <br /><br>';
								
								if($without_card == 1){
									$html .= '<br><br><br><br><br><br><br><br><br><br><br>';
								}
								
								$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" style="display:'.$disp.'">
								 <tr>
								  <td colspan="2">Please find below e-health card for your reference. <br><br></td>
								 </tr>
                                  <tr>
                                    <td width="50%" valign="middle">
										<table id="dynamic_content">
										 <tr><td width="25px"> &nbsp;</td><td align="middle"> <br><br><font size="8pt;" face="Gill Sans MT Pro Heavy"><span style="marign-left:30px;color:#fff; font-weight: bold;"><br>'.$pdf_data_arr[0].' <br>'.$pdf_data_arr[1].'</span></font></td></tr>
										 <tr><td width="25px"> &nbsp;</td><td><br><br><font size="6pt;" face="Gill Sans MT Pro Heavy"><span style="marign-left:30px;color:#fff; font-weight: bold;">Client ID <br>'.$pdf_data_arr[2].'</span></font></td></tr>
										 <tr><td width="25px"> &nbsp;</td><td><br><font size="6pt;" face="Gill Sans MT Pro Heavy"><span style="marign-left:30px;color:#fff; font-weight: bold;">DOB <br>'.$pdf_data_arr[3].'</span></font></td></tr>
										
										</table>
									</td>
                                    <td width="50%" align="right" valign="middle"><img src="pdf/Card_2.jpg" width="185" border="0"></td>
									
                                  </tr>
                                </table>
                                <font size="9pt;" face="Gill Sans MT Pro Heavy"><br>
                                <br />
								<br />
								<br />
                                Team Care Health Insurance
                                <br />
                                Kindly note that this is a computer generated document and hence no signature is required.<br><br></font></font>
                                </td>
                            </tr>  
<table cellpadding="0" cellspacing="0" style="margin:0 auto;background:#fff;" width="100%" >		
 <tr><td width="60%"></td>
            <td width="30%" align="left"  > <img src="img/reach-us.jpg"> </td>
			<td width="10%"></td>
        </tr>					
                             <tr>
				  <td width="50%" style="font-family:Gotham,Helvetica, Arial, sans-serif; font-size:7pt; line-height:1.4; font-weight:normal; color:#414042; text-align:left;"><span style="font-family:Gotham,, Helvetica, Arial, sans-serif; font-size:7pt; line-height:1.4; font-weight:normal; color:#414042; font-weight: bold;">Care Health Insurance Limited</span><br>
					(Formerly Religare Health Insurance Company Limited)
					Regd. <br> Office: 5th Floor, 19 Chawla House, Nehru Place, New Delhi- <br>110019
					Corresp. Office: Unit No. 604 - 607, 6th Floor, Tower C,<br> Unitech Cyber Park,
					Sector-39, Gurugram -122001 (Haryana)<br>
					<span style="font-size: 7pt;"><strong>IRDAI Regn. No. 148</strong></span>    |    CIN: U66000DL2007PLC161503</td>
				  <td width="50%" style="color: #414042; border-top:1px solid #1169B0; border-left:1px solid #1169B0;border-right:1px solid #1169B0; border-bottom:1px solid #1169B0; padding:10px;">
					 <table width="100%">                 				 
						<tr>						
						  <td width="20%" style="font-family:Gotham,Helvetica, Arial, sans-serif; font-size:7pt; line-height:1.4; font-weight:normal; color:#414042; text-align:left;"><br><br><a href="https://bit.ly/31HjeE0" target="_blank"><img src="img/mobile-app.jpg"></a></td>
						  <td width="25%" style="font-family:Gotham, Helvetica, Arial, sans-serif; font-size:7pt; line-height:1.4; font-weight:normal; color:#414042; text-align:left;"><br><br><a href="https://bit.ly/31HjeE0" target="_blank"><img src="img/whatsapp.jpg"></a></td>
						  <td width="55%" style="color: #414042; font-family:Gotham, Helvetica, Arial, sans-serif; font-size:7pt; line-height:1.4; font-weight:normal; color:#414042; text-align:left;"><strong>Self Help Portal:</strong> <a href="https://www.careinsurance.com/self-help-portal.html" target="_blank">www.careinsurance.com/self-help-portal.html </a><br>
							<span> <img src="img/hair-line.png"></span> <strong>Submit Your Queries/Requests: </strong> <a href="https://www.careinsurance.com/contact-us.html" target="_blank">www.careinsurance.com/contact-us.html</a> </td>
						</tr>                 
					</table></td>
				</tr></table>
                              
                                  </table></td>
                                </tr>
                              </table></td>
                            </tr>
                        </table>
                    </td>
                </tr> 
            </table>

        </body>
    </html>';
	
	//echo $html; die;

?>
