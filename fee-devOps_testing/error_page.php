<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Care Health Insurance</title>
        <link rel="stylesheet" type="text/css" href="<?php echo PORTALURL;?>css/style.css"/>
        <script type="text/javascript" src="<?php echo PORTALURL;?>js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo PORTALURL;?>js/select_menu.js"></script>
        <script type="text/javascript" src="<?php echo PORTALURL;?>js/placeholders.min.js"></script>
		<style type='text/css'>
		.error{
			color:red;text-align:center;font-weight:bold;
		}
		</style>
    </head>
    <body>
        <?php include 'inc/inc_header.php'; ?>
		<div class="mid_container">
            <div class="quoteBoxgreen"></div>
            <div class="quoteBoxgreenBottom">
			<p class="error"><?php echo $msg;?></p>
			</div>
            <div class="cl"></div></div>
            <?php include 'inc/inc_footer.php'; ?>
    </body>
</html>