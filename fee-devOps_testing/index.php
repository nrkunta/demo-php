<?php
include_once 'inc/config.php';
include_once 'function.php';
$policyNumber           = isset($_REQUEST['policyNumber']) ? sanitize_data($_REQUEST['policyNumber']) : '';
$productType            = isset($_REQUEST['productType']) ? strtolower(sanitize_data($_REQUEST['productType'])) : '';
$source                 = isset($_REQUEST['source']) ? strtolower(sanitize_data($_REQUEST['source'])) : '';
$zreqnum                = isset($_REQUEST['zreqnum']) ? sanitize_data($_REQUEST['zreqnum']) : '';
$clntnum                = isset($_REQUEST['clntnum']) ? sanitize_data($_REQUEST['clntnum']) : '';
$endorsement_done_by    = isset($_REQUEST['endorsement_done_by']) ? sanitize_data($_REQUEST['endorsement_done_by']) : '';
$agent_id               = isset($_REQUEST['agent_id']) ? sanitize_data_email($_REQUEST['agent_id']) : '';
$plan                   = isset($_REQUEST['plan']) ? sanitize_data($_REQUEST['plan']) : '';
$session_id             = isset($_REQUEST['session_id']) ? sanitize_data($_REQUEST['session_id']) : '';
$endorsmentType         = isset($_REQUEST['endorsmentType']) ? sanitize_data($_REQUEST['endorsmentType']) : '';
$dropdown_array         = array();
$productTypeArray       = array('health', 'travel');
$sourceArray            = array('propero', 'faveo', 'crm');
//in case of crm only
$ChangeTo               = isset($_REQUEST['ChangeTo']) ? sanitize_data_address($_REQUEST['ChangeTo']) : '';
include_once 'get_master_data.php';
//temporary check for crm only
if (!empty($endorsmentType)) {
    $session_id = 'crm';
}

//check mandatory fields for phase2
if (empty($endorsmentType))
    if (empty($policyNumber) || (!in_array($productType, $productTypeArray)) || (!in_array($source, $sourceArray)) || empty($plan) || empty($session_id)) {
        $msg = "You do not have permission to access this page.Please contact to Administrator.";
        include_once('error_page.php');
        exit;
    }

//include files based on source.	
if (!empty($source)) {
    switch ($source) {
        case 'propero': {
                //check if zreqnum is empty then redirect
                if (empty($zreqnum)) {
                    $msg = "You do not have permission to access this page.Please contact to Administrator.";
                    include_once('error_page.php');
                    exit;
                }
                include_once('index_fav_pro.php');
                break;
            }
        case 'faveo': {
                include_once('index_fav_pro.php');
                break;
            }
        case 'crm': {
                if (empty($zreqnum)) {
                    $msg = "You do not have permission to access this page.Please contact to Administrator.";
                    include_once('error_page.php');
                    exit;
                }
                include_once('index_crm.php');
                break;
            }
    }
}
