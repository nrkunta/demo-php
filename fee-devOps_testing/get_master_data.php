<?php
include_once 'inc/config.php';
include_once 'function.php'; 
include_once 'api/apiObject2Array.php';
include_once 'api/xml2array.php';
include_once('api/api_prop.php');
include_once("api/domxml-php4-to-php5.php");

//get master data for relationship
function get_master_data($input){
	$xmlReq='<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:rel="http://relinterface.insurance.symbiosys.c2lbiz.com" xmlns:xsd="http://intf.insurance.symbiosys.c2lbiz.com/xsd">
	   <soap:Header/>
	   <soap:Body>
		  <rel:getMasterData>
			 <!--Optional:-->
			 <rel:intGetPolicyIO>
				<xsd:key>'.$input.'</xsd:key>
			 </rel:intGetPolicyIO>
		  </rel:getMasterData>
	   </soap:Body>
	</soap:Envelope>';				
	$response=soapReq($xmlReq , 'getMasterData');
	$result =  new  xml2array($response);   
	$resultData=$result->getResult();	
	$master_data=array();
	if(isset($resultData['soapenv:Envelope']['soapenv:Body']['ns:getMasterDataResponse']['ns:return']['int-get-master-data']['master-list'])){
	$dataval=$resultData['soapenv:Envelope']['soapenv:Body']['ns:getMasterDataResponse']['ns:return']['int-get-master-data']['master-list'];
	
		if(!empty($dataval)){
			if($input != 'PARTYRELATION'){
			 $master_data[$dataval['key']['#text']]=$dataval['desc']['#text'];
			}
				$i=0;
				foreach($dataval as $val){
					if($i>2){
							$master_data[$val['key']['#text']]=$val['desc']['#text'];
						}
						$i++;
				}
		}
	}
	return $master_data;
}
if($source=='faveo'){
    $nominee_relationship=$relations_faveo;//Getting faveo relations from config file
    $sponsor_relationship=get_master_data('PARTYRELATION');
}else if($source=='crm'){
    $nominee_relationship=$relations_crm;//Getting CRM Nominee relations from config file
    $sponsor_relationship=$relations_crm;////Getting CRM Sponsor relations from config file
}else{
    $nominee_relationship=get_master_data('NOMINEERELATION');
    $sponsor_relationship=get_master_data('PARTYRELATION');
}
