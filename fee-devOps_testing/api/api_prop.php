<?php	
function soapReq($xmlReq, $action, $dir=null){
	$version = SOAP_1_2;
	$location=PROPEROENDPOINT; 
	$wsdl=PROPEROWSDL;
    $opts = array(
	//    'trace' => true,
	//    'exceptions' => 1,
	//    'style' => SOAP_DOCUMENT,
	//    'use' => SOAP_LITERAL ,
	//    'soap_version' => SOAP_1_2 
    );
    $objSoapClient = new SoapClient($wsdl, $opts); 
    $response = $objSoapClient->__doRequest($xmlReq, $location, $action, $version);
	return $response;
}
?>