<?php 
include_once('tcpdf_include.php');
// Extend the TCPDF class to create custom Header and Footer
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Prashant Kumar Gupta');
$pdf->SetTitle('Care Health Insurance');
$pdf->SetSubject('');
$pdf->SetKeywords('Care, Endosement');
// set default header data

$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.'', PDF_HEADER_STRING);
//$pdf->setFooterData('fdhdfhfghfghfg', 'fghfghfghfghfhf');

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set image scale factor
//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
//$fontname = $pdf->addTTFfont('/fonts/gillsansmtprolight_0.ttf', 'TrueTypeUnicode', '', 32);
//$pdf->SetFont('gillsansmtprolight', '', 9, '', 'true');
$pdf->SetFont('dejavusans', '', 9);
$pdf->AddPage();
//$img_file = K_PATH_IMAGES.'Card_1.jpg';
if($show_card == 1){
$img_file = 'pdf/Card_1.jpg';
$pdf->Image($img_file, 18, 175, 77, 50, '', '', '', false, 300, '', false, false, 0);
}

?>