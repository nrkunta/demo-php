<?php
//get case id
function get_case_id(){
	    date_default_timezone_set('UTC');
		$micro_date = microtime();
		$date_array = explode(" ",$micro_date);
		$ms_array=explode("." ,round($date_array[0] , 3));
		$randstring=mt_rand(10,100).$ms_array[1];
		$zreqnum = date('ymdhs').$randstring;
        return $zreqnum;		
}


//get time format
 function get_timeformat($timestamp , $type){
	 if(!empty($timestamp)){
		 
	 if(($type == 'nominee') || ($type == 'sponsor')){
		 if(strpos($timestamp , 'T0') !== false){
			 $dateformat=explode('T0' , $timestamp);
	         return date('d/m/Y' , strtotime($dateformat[0]));
		 }else{
			 //return $timestamp; 
			 $timestamp = strtr($timestamp, '/', '-');
			 return date('d/m/Y' , strtotime($timestamp));
		 }
	 }else if(($type == 'pdf')){
		 if(strpos($timestamp , 'T0') !== false){
			 $dateformat=explode('T0' , $timestamp);
	         return date('d-M-Y' , strtotime($dateformat[0]));
		 }else{
			  $timestamp = strtr($timestamp, '/', '-');
			  return date('d-M-Y' , strtotime($timestamp));
		 }
	 }else{
		 if(strpos($timestamp , 'T0') !== false){
			 $dateformat=explode('T0' , $timestamp);
	         return date('Ymd' , strtotime($dateformat[0]));
		 }else{
			  $timestamp = strtr($timestamp, '/', '-');
			  return date('Ymd' , strtotime($timestamp));
		 }
	 }
	 }else{
		 return '';
	 }
 }

