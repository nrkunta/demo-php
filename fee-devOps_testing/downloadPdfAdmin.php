<?php
include_once('api/xml2array.php');
include_once("api/domxml-php4-to-php5.php");
include_once("admin/inc/db_config.php");
include_once("inc/config.php");
include_once('api/api_prop.php');
include_once('function.php');
$zreqnum = 	mysqliRealEscapeString(base64_decode($_REQUEST['id']));

$endData = fetchDataByZreqNum('endorsement_reports', $zreqnum);

$endorsmentType = $endData[0]['endorsmentType'];
$policyNumber = $endData[0]['policyNumber'];
$strCaseNumber = $endData[0]['zreqnum'];
$customerId = $endData[0]['customerid'];
$currentDate = date("d-M-Y");
$oldvalue = $endData[0]['old_value'];
$newValue = $endData[0]['new_value'];


/* getting new value */
$xmlData = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:rel="http://relinterface.insurance.symbiosys.c2lbiz.com"
					xmlns:xsd="http://intf.insurance.symbiosys.c2lbiz.com/xsd">
					<soap:Header/>
					<soap:Body>
					  <rel:getPolicyDetails>
						 <rel:intGetPolicyIO>
							<xsd:policyNum>' . $policyNumber . '</xsd:policyNum>
						 </rel:intGetPolicyIO>
					  </rel:getPolicyDetails>
				   </soap:Body>
				</soap:Envelope>';
file_put_contents("data/policy_data/" . $policyNumber . '-' . $endorsmentType . "_Request.xml", $xmlData);
$response = soapReq($xmlData, 'getPolicyDetails');
file_put_contents("data/policy_data/" . $policyNumber . '-' . $endorsmentType . "_Response.xml", $response);
$xml = new xml2array($response);
$dataArr = $xml->getResult();
if (isset($dataArr['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['error-lists']['err-description']['#text'])) {
    if (strpos($dataArr['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['error-lists']['err-description']['#text'], 'Unable to') !== false) {
        $error = "1## Please check the policy status";
    } else {
        $error = "1##" . $dataArr['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['error-lists']['err-description']['#text'];
    }
} else if (isset($resultData['soapenv:Envelope']['soapenv:Body']['soapenv:Fault']['soapenv:Code']['soapenv:Value']['#text'])) {
    $error = '1## Sorry Webservice not available right now.Please try later';
} else {
    $data = @$dataArr['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['policy'];
}
//echo $error;
if(!empty($error)){
    echo 'Unable to download pdf.Please try after some time';exit;
}
$firstName1 = @$data['list-party-dOList']['first-name1']['#text'];
$lastName1 = @$data['list-party-dOList']['last-name1']['#text'];
$titleCd = @$data['list-party-dOList']['title-cd']['#text'];
$firstLastName = ucfirst(strtolower(@$data['list-party-dOList']['title-cd']['#text'])) . ' ' . @$data['list-party-dOList']['first-name1']['#text'] . ' ' . @$data['list-party-dOList']['last-name1']['#text'];
$addressLine1Lang1New = @$data['list-party-dOList']['list-party-address-dOList']['address-line1-lang1']['#text'];
$addressLine2Lang1New = @$data['list-party-dOList']['list-party-address-dOList']['address-line2-lang1']['#text'];
$stateCdNew = @$data['list-party-dOList']['list-party-address-dOList']['state-cd']['#text'];
$cityCdNew = @$data['list-party-dOList']['list-party-address-dOList']['city-cd']['#text'];
$areaCdNew = @$data['list-party-dOList']['list-party-address-dOList']['area-cd']['#text'];


//**********************Dates***********************
$cond = " AND policyNumber='".$policyNumber."' AND zreqnum='".$zreqnum."'";
$report_data=fetchReportList('endorsement_reports',$cond,0,1); 
if($report_data[0]['viewDate']!=''){
    $Date = explode(" ", $report_data[0]['viewDate']);
}else{
    $Date = explode(" ", $report_data[0]['endorsmentDate']);
}

$endorsementDate = date("d M Y",strtotime($Date[0]));
$startDate = get_timeformat(@$data['policy-commencement-dt']['#text'], 'commencement');
if (strtotime($endorsementDate) > strtotime($startDate)) {
    $effDate = $endorsementDate;
} else {
    $effDate = date("d M Y",strtotime($startDate));
}

//echo $effDate."-".$endorsementDate;exit;
//**************************************************
/* if the value is n/a  , empty value is showing in pdf as a part of address change issue -prop3650  */
if ($areaCdNew == 'N/A') {
    $areaCdNew = "";
}

if ($areaCd == 'N/A') {
    $areaCd = "";
}

$pinCodeNew = @$data['list-party-dOList']['list-party-address-dOList']['pin-code']['#text'];
$productFamily = @$data['base-product-family-cd']['#text'];

$contactNumNew = @$data['list-party-dOList']['list-party-contact-dOList']['contact-num']['#text'];
$keyMod = '';
$user_name = ucfirst(strtolower($titleCd)) . ' ' . $firstName1 . ' ' . $lastName1;
$newData = ucfirst(strtolower($titleCd)) . ' ' . $firstName1 . ' ' . $lastName1;

if ($endorsmentType == '15' || $endorsmentType == '01') {
    $show_card = 1;
} else {
    $show_card = 0;
}
if($customerId==''){
    $customerId = $data['list-party-dOList']['customer-id']['#text'];
}
$dob = get_timeformat($data['list-party-dOList']['birth-dt']['#text'], 'pdf');
$pdf_name_data = $data['list-party-dOList']['first-name1']['#text'] . "@@@" . $data['list-party-dOList']['last-name1']['#text'] . "@@@" . $customerId . "@@@" . $dob;

if ($show_card == 0) {
    $disp = 'none';
    $without_card = 1;
} else {
    $disp = '';
    $pdf_data_arr = explode("@@@", $pdf_name_data);
    $without_card = 0;
}
if ($endorsmentType == '15' || $endorsmentType == '01') {
    $user_name = $newData;
} else {
    $user_name = ucfirst(strtolower($titleCd)) . ' ' . $firstName1 . ' ' . $lastName1;
}
$keyMod = $endorsmentDropDown[$endorsmentType];

if (isset($oldvalue) && !empty($oldvalue)) {
    $oldData = '';
    //Check string contains value , or @$ symbols
    if ((strpos($oldvalue,',') !== false) || (strpos($oldvalue,'@$') !== false)) {
        if ($endorsmentType == '08') {
			if(strpos($oldvalue,',') !== false){
				$old_value_arr = explode(',', $oldvalue);
			}else{
				$old_value_arr = explode('@$', $oldvalue);
			}
			            
            $addressLine1Lang1New = @$data['list-party-dOList']['list-party-address-dOList'][0]['address-line1-lang1']['#text'];
            $addressLine2Lang1New = @$data['list-party-dOList']['list-party-address-dOList'][0]['address-line2-lang1']['#text'];
            $stateCdNew = @$data['list-party-dOList']['list-party-address-dOList'][0]['state-cd']['#text'];
            $cityCdNew = @$data['list-party-dOList']['list-party-address-dOList'][0]['city-cd']['#text'];
            $areaCdNew = @$data['list-party-dOList']['list-party-address-dOList'][0]['area-cd']['#text'];
            $pinCodeNew = @$data['list-party-dOList']['list-party-address-dOList'][0]['pin-code']['#text'];
        } else {
            $old_value_arr = explode(',', $oldvalue);
        }
		
        if (!empty($old_value_arr)) {
            for ($i = 0; $i < count($old_value_arr); $i++) {
                //Check string contains value : symbols
                if (strpos($old_value_arr[$i],':') !== false){					
                     $old_value_str_arr = explode(':', $old_value_arr[$i]);
                     if($endorsmentType=='01' || $endorsmentType=='15'){
                         $oldData .= $old_value_str_arr[1] . ' ';
                     }else if($endorsmentType=='05'){
                        if($i==0 ||$i==1||$i==4){
                            $oldData .= $old_value_str_arr[1] . ' ';
                        }else{
                            $oldData .= $old_value_str_arr[1] . ',';
                        }
                    }else{
                         $oldData .= $old_value_str_arr[1] . ',';
                     }
                }elseif($endorsmentType == '08' && (strpos($old_value_arr[$i],':') === false)){
					$oldData .= $old_value_arr[$i] . ',';
					
				}else{					
                     $oldData .='';
                }
               
            }
        }
    } else {
        if (strpos($oldvalue,':') !== false){
             //Check string contains value ':' symbols
            $old_value_str_arr = explode(':', $oldvalue);
            $oldData=$old_value_str_arr[1];
        }else{
            $oldData=$oldvalue;
        }
        
    }
} else {
    $oldData = $oldvalue;
}

if (isset($newValue) && !empty($newValue)) {
    $newData = '';
     //Check string contains value , or @$ symbols
    if ((strpos($newValue,',') !== false) || (strpos($newValue,'@$') !== false)) {
        if ($endorsmentType == '08') {
            $new_value_arr = explode('@$', $newValue);
        } else {
            $new_value_arr = explode(',', $newValue);
        }
        
        if (!empty($new_value_arr)) {
            for ($i = 0; $i < count($new_value_arr); $i++) {
                if($endorsmentType=='01' || $endorsmentType=='15'){
                    $newData .= $new_value_arr[$i] . ' ';
                }else if($endorsmentType=='05'){
                    if($i==0 ||$i==1||$i==4){
                        $newData .= $new_value_arr[$i] . ' ';
                    }else{
                        $newData .= $new_value_arr[$i] . ',';
                    }
                }else{
                    $newData .= $new_value_arr[$i] . ',';
                }
            }
        }
    }else{
        $newData=$newValue;
    }
} else {
    $newData = $newValue;
}

if($endorsmentType=='01' || $endorsmentType=='15'){
    $newData= ucfirst(strtolower($titleCd)).' '. $newData;
    $oldData= ucfirst(strtolower($titleCd)).' '. $oldData;
    
}
include_once('pdf.php');
include_once 'pdftohtml.php';
$pdf_fileName = $policyNumber . '-' . time();
$fileName = $pdf_fileName . '.pdf';
ob_clean();
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output($fileName,'D');
exit;
?>
