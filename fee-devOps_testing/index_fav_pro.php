<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta name="Referrer?Policy" value="no?referrer | same?origin"/>
        <title>Religare</title>
        <link rel="stylesheet" type="text/css" href="<?php echo PORTALURL;?>css/style.css"/>
        <link rel="stylesheet" href="css/jquery-ui.css?v=1">
            <script type="text/javascript" src="js/jquery.min.js?v=1"></script>
            <script type="text/javascript" src="js/jquery-ui.js?v=1"></script>
            <script type="text/javascript">

                jQuery(document).ready(function ($) {
                    $("#nominee_dob , #sponosor_dob , #sponosor_dob_value , #nominee_dob_value").datepicker({
                        dateFormat: 'dd/mm/yy',
                        changeMonth:true,
			changeYear:true,
                        yearRange: '1900:' + new Date().getFullYear(),
                        maxDate: new Date()
                    });
                });


                function endorsementTest() {
                    var policyNumber = $("#policyNumber").val();
                    var endorsmentType = $("#endorsmentType option:selected").val();

                    if (endorsmentType == '0') {
                        alert('Please select endorsement type');
                        return false;
                    }

                    var deployment = '<?php echo $source; ?>';
                    /* alert("deployment"+deployment); */
                    var zreqnum = '<?php echo $zreqnum; ?>';
                    var productType = '<?php echo $productType; ?>';
                    var token=$("#csrf_token").val();
                    if (endorsmentType == '15') {
                        var customerId = "wt";
                        var endorDataRequest = "policyNumber=" + policyNumber + "&endorsmentType=" + endorsmentType + "&customerId=" + customerId + "&deployment=" + deployment + "&case_id=" + zreqnum+"&token=" + token;
                    } else {
                        var endorDataRequest = "policyNumber=" + policyNumber + "&endorsmentType=" + endorsmentType + "&deployment=" + deployment + "&case_id=" + zreqnum+"&token=" + token;
                    }
                    jQuery.ajax({
                        /*headers: {
                         Accept: "text/plain; charset=utf-8",
                         "Content-Type": "text/plain; charset=utf-8"
                         },*/
                        type: "POST",
                        url: "getData.php",
                        async: true,
                        data: endorDataRequest,
                        beforeSend: function (req) {
                            req.setRequestHeader("Accept", "text/xml");
                            $("#PolicyInsuredName,#policyDetailData,#policyDetailDataloaderthanku").hide();
                            $("#policyDetailDataloader").show();
                        },
                        success: function (msg) {
                            var res = msg.split("##");
                            if (res[0] == '1') {
                                $("#policyDetailDataloaderModify,#policyDetailDataloader").hide();
                                $("#policyDetailDataloaderthanku").show();
                                $("#errormsg").html("<span style='color:blue'>" + res[1] + "</span>");
                            } else {

                                $("#modifyButton").show();
                                var t = res[1].split(":");
                                var old_values = t[0];
                                var old_value_arr = old_values.split("|");
                                var customer_id = t[1];
                                var endorsementtype = t[2];
                                var zreqnum = t[3];
                                var policy_status = t[4];
                                var policy_effective_date = t[5];

                                $("#zreqnum").val(zreqnum);
                                $("#cust_id").val(customer_id);
                                if (endorsementtype != '15') {
                                    var member_id = t[6];
                                    $('#mem_id').val(member_id);
                                }

                                $("#policy_status").val(policy_status);
                                $("#policy_effective_date").val(policy_effective_date);
                                $('.policy_fields').hide();
                                //$('#name_note').hide();
                                //policy holder name
                                if (endorsementtype == '01') {
                                    $('#policyholder_details').show();
                                    $('#policyholder_first_name').val(old_value_arr[0]);
                                    $('#policyholder_last_name').val(old_value_arr[1]);
                                    //$('#policyholder_first_name_value').val(old_value_arr[0]);
                                    //$('#policyholder_last_name_value').val(old_value_arr[1]);
                                    //$('#name_note').show();
                                }

                                //email
                                else if (endorsementtype == '12') {
                                    $('#email_details').show();
                                    $('#email').val(old_values)
                                }

                                //phone no
                                else if (endorsementtype == '11') {
                                    $('#phone_details').show();
                                    $('#phoneNo').val(old_values);
                                }

                                //pan details
                                else if (endorsementtype == '20') {
                                    $('#pan_details').show();
                                    $('#panNo').val(old_values);
                                }

                                //passport_details
                                else if (endorsementtype == '21') {
                                    $('#passport_details').show();
                                }

                                // maiden_name
                                else if (endorsementtype == '16') {
                                    $('#maiden_name').show();
                                    $('#mother_maiden_name').val(old_values);
                                }

                                // course name
                                else if (endorsementtype == '32') {
                                    $('#course_name').show();
                                    $('#cours_name').val(old_values);
                                }


                                //university details
                                else if (endorsementtype == '31') {
                                    $('#university_details').show();
                                    $('#university_name').val(old_value_arr[0]);
                                    $('#university_address').val(old_value_arr[1]);
                                }

                                //sponsor details
                                else if (endorsementtype == '30') {
                                    $("#sponsor_detail").show();
                                    $("#sponosor_name").val(old_value_arr[0]);
                                    $("#sponosor_dob").val(old_value_arr[1]);
                                    $("#sponsor_relation option[value='" + old_value_arr[2] + "']").attr("selected", "selected");
                                }

                                // martial status
                                else if (endorsementtype == '23') {
                                    $('#martial_stat').show();
                                    $("#martial_status option[value='" + old_values + "']").attr("selected", "selected");
                                }

                                //nominee details
                                else if (endorsementtype == '05') {
                                    $('#nominee_detail').show();
                                    $("#nominee_title option[value='" + old_value_arr[0] + "']").attr("selected", "selected");
                                    $("#nominee_first_name").val(old_value_arr[1]);
                                    $("#nominee_last_name").val(old_value_arr[2]);
                                    $("#nominee_dob").val(old_value_arr[3]);
                                    $("#nominee_relation option[value='" + old_value_arr[4] + "']").attr("selected", "selected");
                                }


                                //address details
                                else if (t[2] == '08') {
                                    $('#address_details').show();
                                    $('#address1').val(old_value_arr[0]);
                                    $('#address2').val(old_value_arr[1]);
                                    $('#city').val(old_value_arr[2]);
                                    $('#locality').val(old_value_arr[3]);
                                    $('#state').val(old_value_arr[4]);
                                    $('#pincode').val(old_value_arr[5]);
                                }

                                //insured name
                                else if (t[2] == '15') {
                                    $('#PolicyInsuredName').show();
                                    if (t[0] == '' || t[0] == 'N/A') {

                                        $("#policyDetailDataloaderModify,#policyDetailDataloader").hide();
                                        $("#policyDetailDataloaderthanku").show();
                                        $("#errormsg").html("<span style='color:red'>No insured found!</span>");
                                        return false;
                                    }

                                    var P = t[0].split("|");
                                    //For Policy Insured Name All Details
                                    var PINAD = '';
                                    var PINL = '';
                                    var PINAD1 = '';
                                    var PINL1 = '';
                                    var PINAD2 = '';
                                    var PINL2 = '';
                                    var PINAD3 = '';
                                    var PINL3 = '';
                                    var PINAD4 = '';
                                    var PINL4 = '';
                                    if (P[0]) {
                                        var PINAD = P[0].split("_");
                                        var PINL = PINAD[0];
                                        var PCIDADIVALSL = PINAD[1].split("DI");
                                        var PDIVALSL = PCIDADIVALSL[1];
                                    }
                                    if (P[1]) {
                                        var PINAD1 = P[1].split("_");
                                        var PINL1 = PINAD1[0];
                                        var PCIDADIVALSL1 = PINAD1[1].split("DI");
                                        var PDIVALSL1 = PCIDADIVALSL1[1];
                                    }
                                    if (P[2]) {
                                        var PINAD2 = P[2].split("_");
                                        var PINL2 = PINAD2[0];
                                        var PCIDADIVALSL2 = PINAD2[1].split("DI");
                                        var PDIVALSL2 = PCIDADIVALSL2[1];
                                    }
                                    if (P[3]) {
                                        var PINAD3 = P[3].split("_");
                                        var PINL3 = PINAD3[0];
                                        var PCIDADIVALSL3 = PINAD3[1].split("DI");
                                        var PDIVALSL3 = PCIDADIVALSL3[1];
                                    }
                                    if (P[4]) {
                                        var PINAD4 = P[4].split("_");
                                        var PINL4 = PINAD4[0];
                                        var PCIDADIVALSL4 = PINAD4[1].split("DI");
                                        var PDIVALSL4 = PCIDADIVALSL4[1];
                                    }

                                    var sel_str = 'Select';
                                    opt_val = ' <select class="styled" name="PolicyInsuredName" id="PolicyInsuredName" style="width:235px;" onChange="getPolicyInsuredName(this.value)">';
                                    opt_val += '<option value="" selected>' + sel_str + '</option>';
                                    if (P[0]) {
                                        opt_val += '<option value="' + P[0] + '" >' + PINL + '</option>';
                                    }

                                    if (P[1]) {
                                        opt_val += '<option value="' + P[1] + '" >' + PINL1 + '</option>';
                                    }

                                    if (P[2]) {
                                        opt_val += '<option value="' + P[2] + '" >' + PINL2 + '</option>';
                                    }

                                    if (P[3]) {
                                        opt_val += '<option value="' + P[3] + '" >' + PINL3 + '</option>';
                                    }

                                    if (P[4]) {
                                        opt_val += '<option value="' + P[4] + '" >' + PINL4 + '</option>';
                                    }
                                    opt_val += ' </select>';
                                    $("#InsuredNameList").html(opt_val);
                                    $("#case_id").hide();
                                    $("#modifyButton").hide();
                                }

                                $("#policyDetailDataloader").hide();
                                $("#policyDetailData").show();
                            }
                        }
                    });
                }

                function getPolicyInsuredName(val) {
                    if (val != "") {
                        //For Policy Insured Name All Details
                        var str = val.split("_");

                        var name = str[0];
                        var ids = str[1];

                        var name_arr = name.split("-");
                        var fname = name_arr[0];
                        var lname = name_arr[1];

                        var ids_arr = ids.split("@@@@");

                        var mem_id = ids_arr[1];

                        var cust_dep_id_arr = ids_arr[0].split('DI');
                        var cust_id = cust_dep_id_arr[0];
                        var dependent_id = cust_dep_id_arr[1];

                        $('.insured_detail').show();
                        $('#insured_first_name').val(fname);
                        $('#insured_last_name').val(lname);

                        var deployment = '<?php echo $source; ?>';
                        var zreqnum = '<?php echo $zreqnum; ?>';

                        if (deployment == 'faveo') {
                            var c_id =<?php echo get_case_id(); ?>;
                }
                        else {
                            var c_id = zreqnum;
                        }
                        $('#zreqnum').val(c_id);
                        $('#cust_id').val(cust_id);
                        $('#dep_id').val(dependent_id);
                        $('#mem_id').val(mem_id);
                        $("#case_id").show();
                        //$("#name_note").show();
                        $("#modifyButton").show();
                    } else {
                        $('.insured_detail').hide();
                        $("#case_id").hide();
                        $("#modifyButton").hide();
                        //$("#name_note").hide();
                    }
                }

                function isCharsInBag(e, t) {
                    var n;
                    for (n = 0; n < e.length; n++) {
                        var r = e.charAt(n);
                        if (t.indexOf(r) == -1)
                            return false;
                    }
                    return true;
                }

                function checkOnlyDigits(e) {
                    e = e ? e : window.event;
                    var charCode = e.which ? e.which : e.keyCode;
                    if ((charCode < 46 || charCode > 57) && (charCode != 8)) {
                        alert("Only digits allowed!")
                        return false;
                    }
                }


                function valiDateName(oldVal1, oldVal2, newVal1, newVal2) {
                    var oldVal1Arr = oldVal1.split('');
                    var oldVal2Arr = oldVal2.split('');
                    var newVal1Arr = newVal1.split('');
                    var newVal2Arr = newVal2.split('');
                    var oldLength1 = oldVal1Arr.length;
                    var oldLength2 = oldVal2Arr.length;
                    var newLength1 = newVal1Arr.length;
                    var newLength2 = newVal2Arr.length;

                    var diff1 = newLength1 - oldLength1;
                    var diff1 = Math.abs(diff1);

                    var diff2 = newLength2 - oldLength2;
                    var diff2 = Math.abs(diff2);

                    if (diff1 > 2) {
                        alert("You can't change more than two characters of first name");
                        return false;
                    }

                    if (diff2 > 2) {
                        alert("You can't change more than two characters of last name");
                        return false;
                    }

                    if (diff1 <= 2) {
                        var count = 0;
                        for (i = 0; i < newLength1; i++) {
                            if (typeof oldVal1Arr[i] != 'undefined') {
                                if (newVal1Arr[i] != oldVal1Arr[i]) {
                                    count++;
                                }
                            }
                            if (diff1 == 2) {
                                if (count > 0) {
                                    alert("You can't change more than two characters of first name")
                                    return false;
                                }
                            } else if (diff1 == 1) {
                                if (count > 1) {
                                    alert("You can't change more than two characters of first name")
                                    return false;
                                }
                            } else {
                                if (count > 2) {
                                    alert("You can't change more than two characters of first name")
                                    return false;
                                }
                            }
                        }
                    }

                    if (diff2 <= 2) {
                        var count = 0;
                        for (j = 0; j < newLength2; j++) {
                            if (typeof oldVal1Arr[i] != 'undefined') {
                                if (newVal2Arr[j] != oldVal2Arr[j]) {
                                    count++;
                                }
                            }

                            if (diff2 == 2) {
                                if (count > 0) {
                                    alert("You can't change more than two characters of last name")
                                    return false;
                                }
                            } else if (diff2 == 1) {
                                if (count > 1) {
                                    alert("You can't change more than two characters of last name")
                                    return false;
                                }
                            } else {
                                if (count > 2) {
                                    alert("You can't change more than two characters of last name")
                                    return false;
                                }
                            }
                        }
                    }
                }



                // modify data
                function modifyData() {
                    var endorsement_done_by = '<?php echo $endorsement_done_by; ?>';
                    var agent_id = '<?php echo $agent_id; ?>';
                    var policyNumber = $("#policyNumber").val();
                    var endorsmentType = $("#endorsmentType option:selected").val();
                    var clntnum = $("#cust_id").val();
                    var zreqnum = $("#zreqnum").val();
                    var deployment = '<?php echo $source; ?>';
                    var policy_status = $("#policy_status").val();
                    var policy_effective_date = $("#policy_effective_date").val();
                    var productType = '<?php echo $productType; ?>';
                    var member_no = $("#mem_id").val();
                    var dependentId = $("#dep_id").val();
                    var session_id = '<?php echo $session_id; ?>'
                    //policyholder name
                    if (endorsmentType == '01')
                    {
                        var p_first_name = $("#policyholder_first_name").val();
                        var p_last_name = $("#policyholder_last_name").val();
                        var p_first_name_value = $("#policyholder_first_name_value").val();
                        var p_last_name_value = $("#policyholder_last_name_value").val();
                        if (p_first_name_value == "")
                        {
                            alert("Please enter first name");
                            $("#policyholder_first_name_value").focus();
                            return false;
                        } else if (p_last_name_value == "")
                        {
                            alert("Please enter last name");
                            $("#policyholder_last_name_value").focus();
                            return false;
                        } else if ((p_first_name == p_first_name_value) && (p_last_name == p_last_name_value)) {
                            alert("New values should not be same as old values");
                            return false;
                        }

                        var old_value = "First name: " + p_first_name + ", Last name: " + p_last_name;
                        var new_value = p_first_name_value + "," + p_last_name_value;


                        /* var result=valiDateName(p_first_name , p_last_name , p_first_name_value , p_last_name_value);
                         if(result === false){
                         return false;
                         }
                              
                         */


                    }

                    //mother maiden name
                    if (endorsmentType == '16') {
                        var old_v = $('#mother_maiden_name').val();
                        var old_value = "Maiden name: " + $('#mother_maiden_name').val();
                        var new_value = $('#mother_maiden_value').val();
                        if (new_value == '') {
                            alert("Please enter mother maiden name");
                            $("#mother_maiden_value").focus();
                            return false;
                        } else if (old_v == new_value) {
                            alert("New values should not be same as old values");
                            return false;
                        }
                    }


                    //course name
                    if (endorsmentType == '32') {
                        var old_v = $('#cours_name').val();
                        var old_value = "Course name: " + $('#cours_name').val();
                        var new_value = $('#course_value').val();
                        if (new_value == '') {
                            alert("Please enter Course name");
                            $("#course_value").focus();
                            return false;
                        } else if (old_v == new_value) {
                            alert("New values should not be same as old values");
                            return false;
                        }
                    }

                    //rectification in martial status
                    if (endorsmentType == '23') {
                        var old_v = $('#martial_status option:selected').val();
                        var old_value = "Martial Status: " + $('#martial_status option:selected').val();
                        var new_value = $('#martial_status_value option:selected').val();
                        if (new_value == '') {
                            alert("Please enter martial status");
                            $("#martial_status_value").focus();
                            return false;
                        } else if (old_v == new_value) {
                            alert("New values should not be same as old values");
                            return false;
                        }
                    }

                    //change in address
                    if (endorsmentType == '08') {
                        var address1 = $('#address1').val();
                        var address1_value = $('#address1_value').val();
                        var address2 = $('#address2').val();
                        var address2_value = $('#address2_value').val();
                        var locality = $('#locality').val();
                        var locality_value = $('#locality_value').val();
                        var city = $('#city').val();
                        var city_value = $('#city_value').val();
                        var state = $('#state').val();
                        var state_value = $('#state_value').val();
                        var pincode = $('#pincode').val();
                        var pincode_value = $('#pincode_value').val();

                        if (address1_value == '') {
                            alert("Please enter Address1");
                            $("#address1_value").focus();
                            return false;
                        } else {

                            if (address1_value.indexOf('@$') > -1) {
                                alert("Please enter valid value for Address1");
                                return false;
                            }
                        }
                        if (address2_value.indexOf('@$') > -1) {

                            alert("Please enter valid value for Address2");
                            return false;
                        }
                        if (city_value == '') {
                            alert("Please enter City");
                            $("#city_value").focus();
                            return false;
                        } else {

                            if (city_value.indexOf('@$') > -1) {
                                alert("Please enter valid value for City");
                                return false;
                            }
                        }
                        if (state_value == '') {
                            alert("Please enter State");
                            $("#state_value").focus();
                            return false;
                        } else {

                            if (state_value.indexOf('@$') > -1) {
                                alert("Please enter valid value for State");
                                return false;
                            }
                        }
                        if (pincode_value == '') {
                            alert("Please enter Pincode");
                            $("#pincode_value").focus();
                            return false;
                        } else if ((address1 == address1_value) && (address2 == address2_value) && (locality == locality_value) && (city == city_value) && (state == state_value) && (pincode == pincode_value)) {
                            alert("New values should not be same as old values");
                            return false;
                        } else {

                            if (pincode_value.indexOf('@$') > -1) {
                                alert("Please enter valid value for Pincode");
                                return false;
                            }
                        }

                        var old_value = "Address1: " + address1 + "@$ Address2: " + address2 + "@$ Locality: " + locality + "@$ City: " + city + "@$ State: " + state + "@$ Pincode: " + pincode;
                        var new_value = address1_value + "@$" + address2_value + "@$" + locality_value + "@$" + city_value + "@$" + state_value + "@$" + pincode_value;

                    }


                    //university detail
                    else if (endorsmentType == '31') {

                        var university_name = $('#university_name').val();
                        var university_name_value = $('#university_name_value').val();
                        var university_address = $('#university_address').val();
                        var university_address_value = $('#university_address_value').val();

                        if (university_name_value == '') {
                            alert("Please enter University Name");
                            $("#university_name_value").focus();
                            return false;
                        } else if (university_address_value == '') {
                            alert("Please enter University Address");
                            $("#university_address_value").focus();
                            return false;
                        } else if ((university_name == university_name_value) && (university_address == university_address_value)) {
                            alert("New values should not be same as old values");
                            return false;
                        }

                        var old_value = "University name: " + university_name + ",University address: " + university_address;
                        var new_value = university_name_value + "," + university_address_value;
                    }

                    //sponosor detail
                    else if (endorsmentType == '30') {

                        var sponosor_name = $('#sponosor_name').val();
                        var sponosor_name_value = $('#sponosor_name_value').val();
                        var sponosor_dob = $('#sponosor_dob').val();
                        var sponosor_dob_value = $('#sponosor_dob_value').val();
                        var sponsor_relation = $('#sponsor_relation  option:selected').val();
                        var sponsor_relation_value = $('#sponsor_relation_value  option:selected').val();

                        if (sponosor_name_value == '') {
                            alert("Please enter Sponsor Name");
                            $("#sponosor_name_value").focus();
                            return false;
                        } else if (sponosor_dob_value == '') {
                            alert("Please enter Sponsor Dob");
                            $("#sponosor_dob_value").focus();
                            return false;
                        } else if (sponsor_relation_value == '') {
                            alert("Please enter Sponsor Relation");
                            $("#sponsor_relation_value").focus();
                            return false;
                        } else if ((sponosor_name == sponosor_name_value) && (sponosor_dob == sponosor_dob_value) && (sponsor_relation == sponsor_relation_value)) {
                            alert("New values should not be same as old values");
                            return false;
                        }

                        var old_value = "Sponsor name: " + sponosor_name + ", Sponsor dob: " + sponosor_dob + ",Sponsor Relation: " + sponsor_relation;
                        var new_value = sponosor_name_value + "," + sponosor_dob_value + "," + sponsor_relation_value;
                    }

                    //email
                    else if (endorsmentType == '12') {
                        var old_v = $('#email').val();
                        var old_value = "Email:" + $("#email").val();
                        var new_value = $("#email_value").val();
                        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                        if (new_value == "")
                        {
                            alert("Please enter email Id");
                            $("#email_value").focus();
                            return false;
                        } else if (new_value != "" && reg.test(new_value) == false)
                        {
                            alert('Invalid Email Address');
                            $("#new_value").focus();
                            return false;
                        } else if (old_v == new_value) {
                            alert("New values should not be same as old values");
                            return false;
                        }
                    }

                    //phone
                    else if (endorsmentType == '11')
                    {
                        var old_v = $("#phoneNo").val();
                        var old_value = "Phone no: " + $("#phoneNo").val();
                        var new_value = $("#phoneNo_value").val();
                        if (new_value == "" || new_value.length < 10)
                        {
                            alert("Please enter phone no.");
                            $("#phoneNo_value").focus();
                            return false;
                        } else if (!isCharsInBag(new_value, "0123456789")) {
                            alert("Please enter the valid phone no.");
                            $("#ChangeTo").focus();
                            return false;
                        } else if (new_value != "" && new_value.length > 10)
                        {
                            alert("Please enter the valid phone no.");
                            $("#phoneNo_value").focus();
                            return false;
                        } else if (old_v == new_value) {
                            alert("New values should not be same as old values");
                            return false;
                        }

                    }

                    //pan no
                    else if (endorsmentType == '20')
                    {
                        var old_v = $("#panNo").val();
                        var old_value = "Pan no: " + $("#panNo").val();
                        var new_value = $("#panNo_value").val();
                        var panPat = /^([A-Z]{5})(\d{4})([A-Z]{1})$/;
                        if (new_value == "")
                        {
                            alert("Please enter pan no.");
                            $("#panNo_value").focus();
                            return false;
                        } else if (new_value.search(panPat) == -1) {
                            alert("Please enter valid pan no.");
                            $("#panNo_value").focus();
                            return false;
                        } else if (old_v == new_value) {
                            alert("New values should not be same as old values");
                            return false;
                        }
                    }

                    //nominee details
                    if (endorsmentType == '05') {
                        var nominee_title = $('#nominee_title option:selected').val();
                        var nominee_title_value = $('#nominee_title_value option:selected').val();
                        var nominee_first_name = $('#nominee_first_name').val();
                        var nominee_first_name_value = $('#nominee_first_name_value').val();
                        var nominee_last_name = $('#nominee_last_name').val();
                        var nominee_last_name_value = $('#nominee_last_name_value').val();
                        var nominee_dob = $('#nominee_dob').val();
                        var nominee_dob_value = $('#nominee_dob_value').val();
                        var nominee_relation = $('#nominee_relation option:selected').val();
                        var nominee_relation_value = $('#nominee_relation_value option:selected').val();

                        if (nominee_title_value == '') {
                            alert("Please select Title");
                            $("#nominee_title_value").focus();
                            return false;
                        } else if (nominee_first_name_value == '') {
                            alert("Please enter nominee first name");
                            $("#nominee_first_name_value").focus();
                            return false;
                        }

                        //PROP-3650
                        /*else if(nominee_last_name_value == ''){
                         alert("Please enter nominee last name");
                         $("#nominee_last_name_value").focus();
                         return false;
                         }
                         else if(nominee_dob_value == ''){
                         alert("Please enter nominee dob");
                         $("#nominee_dob_value").focus();
                         return false;
                         }
                              
                         else if(nominee_relation_value == ''){
                         alert("Please select Relation");
                         $("#nominee_relation_value").focus();
                         return false;
                         }
                         */

                        else if ((nominee_title == nominee_title_value) && (nominee_first_name == nominee_first_name_value) && (nominee_last_name == nominee_last_name_value) && (nominee_dob == nominee_dob_value) && (nominee_relation == nominee_relation_value)) {
                            alert("New values should not be same as old values");
                            return false;
                        }


                        var old_value = "Nominee Title: " + nominee_title + ", Nominee First name: " + nominee_first_name + ", Nominee Last Name: " + nominee_last_name + ",Nominee dob: " + nominee_dob + ",Nominee Relation: " + nominee_relation;
                        var new_value = nominee_title_value + "," + nominee_first_name_value + "," + nominee_last_name_value + "," + nominee_dob_value + "," + nominee_relation_value;
                    }

                    //insured name
                    if (endorsmentType == '15') {
                        var i_first_name = $("#insured_first_name").val();
                        var i_last_name = $("#insured_last_name").val();
                        var i_first_name_value = $("#insured_first_name_value").val();
                        var i_last_name_value = $("#insured_last_name_value").val();
                        if (i_first_name_value == "")
                        {
                            alert("Please enter first name");
                            $("#insured_first_name_value").focus();
                            return false;
                        }

                        /*else if(insured_last_name_value == "")
                         {
                         alert("Please enter last name");
                         $("#insured_last_name_value").focus();
                         return false;
                         }
                         */

                        else if ((i_first_name == i_first_name_value) && (i_last_name == i_last_name_value)) {
                            alert("New values should not be same as old values");
                            return false;
                        }

                        var old_value = "First name: " + i_first_name + ", Last name: " + i_last_name;
                        var new_value = i_first_name_value + "," + i_last_name_value;

                        /* var result=valiDateName(i_first_name , i_last_name , i_first_name_value , i_last_name_value);
                         if(result === false){
                         return false;
                         }
                         */

                    }
                    var token=$("#csrf_token").val();
                    var dataArray = "policyNumber=" + policyNumber + "&endorsmentType=" + endorsmentType + "&clntnum=" + clntnum + "&endorsement_done_by=" + endorsement_done_by + "&zreqnum=" + zreqnum + "&agent_id=" + agent_id + "&deployment=" + deployment + "&policy_status=" + policy_status + "&policy_effective_date=" + policy_effective_date + "&old_value=" + old_value + "&new_value=" + new_value + "&product_type=" + productType + "&member_no=" + member_no + "&dependentId=" + dependentId + "&session_id=" + session_id+ "&token=" + token;


                    $.ajax({
                        /*  headers: {
                         Accept: "text/plain; charset=utf-8",
                         "Content-Type": "text/plain; charset=utf-8"
                         },
                         */
                        type: "POST",
                        url: "modifyData.php",
                        async: true,
                        data: dataArray,
                        beforeSend: function (req) {
                            req.setRequestHeader("Accept", "text/xml");
                            $("#policyDetailDataloaderthanku,#ViewPDFEmailPDFpolicyDetailDataloaderthanku").hide();
                            $("#policyDetailDataloaderModify").show();
                            $("#modifyButton").hide();
                        },
                        success: function (msg) {
                            $("#searchItem").hide();
                            var t = msg.split("##");
                            if (t[0] == '2') {
                                $('#searchItem').hide();
                                $("#policyDetailDataloaderModify,#policyDetailData").hide();
                                $("#policyDetailDataloaderthanku").show();
                                $("#errormsg").html(t[1]);
                                $("#ChangeTo").val('');
                                $("#ChangeTo1").val('');
                            } else {
                                $("#policyDetailDataloaderModify,#policyDetailData").hide();
                                $("#policyDetailDataloaderthanku").show();
                                $('#searchItem').show();
                                $("#errormsg").html("<span style='color:red'>" + t[1] + "</span>");
                            }
                        }
                    });
                }

                function EmailPDF(a, b, c, d, e, f, g, h, i) {
                    var policyNumber = c;
                    var zreqnum = d;
                    var code = b;
                    var endorsmentType = a;
                    var endorsmentRepostLastId = e;
                    var dependentId = f;
                    var ChangeTo = g;
                    var deployment = h;
                    var customer_id = i;
                    var token=$("#csrf_token").val();
                    $.ajax({
                        /*headers: {
                         Accept: "text/plain; charset=utf-8",
                         "Content-Type": "text/plain; charset=utf-8"
                         },*/
                        type: "GET",
                        url: "emailpdf.php",
                        async: true,
                        data: "policyNumber=" + policyNumber + "&zreqnum=" + zreqnum + "&code=" + code + "&endorsmentType=" + endorsmentType + "&endorsmentRepostLastId=" + endorsmentRepostLastId + "&dependentId=" + dependentId + "&ChangeTo=" + ChangeTo + "&deployment=" + deployment + "&customer_id=" + customer_id+ "&token=" + token,
                        beforeSend: function (req) {
                            req.setRequestHeader("Accept", "text/xml");
                            $("#ViewPDFEmailPDFpolicyDetailDataloaderthanku").hide();
                            $("#policyDetailDataloaderModify").show();
                        },
                        success: function (msg) {
                            $("#policyDetailDataloaderModify").hide();
                            if (deployment != 'crm') {
                                $("#policyDetailDataloaderthanku").show();
                            }
                            $("#ViewPDFEmailPDFpolicyDetailDataloaderthanku").show();
                            $("#ViewPDFEmailPDFpolicyDetailDataloaderthanku").html('<tr><td height="50" align="center">' + msg + '</td></tr>');

                            //$("#errormsg").html(msg);
                            //if(deployment != 'crm'){
                            //$("#policyDetailDataloaderthanku").show();
                            //}*/
                        }
                    });
                }

                // download travel pdf


                function hideModifyDataSet() {
                    $("#ViewPDFEmailPDFpolicyDetailDataloaderthanku").hide();
                    return true;
                }

                function hideModifyDataSetVal() {
                    var deployment = '<?php echo $source; ?>';
                    if (deployment == 'crm') {
                        $("#policyDetailDataloaderthanku").hide();
                    }
                    $("#ViewPDFEmailPDFpolicyDetailDataloaderthanku").show();
                    $("#ViewPDFEmailPDFpolicyDetailDataloaderthanku").html('<tr><td height="50" align="center">Your requested policy detail has been viewed successfully.</td></tr>');
                    return false;

                }

                function onlyAlphabets(e, t) {
                    try {
                        if (window.event) {
                            var charCode = window.event.keyCode;
                        } else if (e) {
                            var charCode = e.which;
                        } else {
                            return true;
                        }
                        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                            return true;
                        else
                            return false;
                    } catch (err) {
                        alert(err.Description);
                    }
                }
				
				function allowDotWithAlphabets(e, t){
					try {
                        if (window.event) {
                            var charCode = window.event.keyCode;
                        } else if (e) {
                            var charCode = e.which;
                        } else {
                            return true;
                        }
                        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode == 46))
                            return true;
                        else
                            return false;
                    } catch (err) {
                        alert(err.Description);
                    }
                }
				
            </script>
    </head>
    <body>
        <?php include 'inc/inc_header.php'; ?>
        <div class="mid_container">
            <div class="quoteBoxgreen"></div>
            <div class="quoteBoxgreenBottom">

                <!-- Endorsement search table start -->
                <table width="959" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="69" height="0" align="right">Policy No. :</td>
                        <td width="0" height="50">&nbsp;</td>
                        <td width="280">
                            <div class="dropdown_otc">
                                <input type="hidden" id="csrf_token" name="csrf" value="<?php echo $_SESSION["token"]; ?>"/>
                                <input type="text" name="policyNumber" AUTOCOMPLETE="OFF" maxlength="8"  id="policyNumber" class="email_f" style="width:168px;" value="<?php echo $policyNumber; ?>" placeholder="Policy No." readonly />
                            </div>
                        </td>
                        <td width="155" align="right">Endorsement Type :</td>
                        <td width="34">&nbsp;</td>
                        <td width="388">
                            <div class="">
                                <select class="styled" value="endorsmentType" id="endorsmentType" style="width:235px;">
                                    <option value="0">Select</option>
                                    <?php
                                    if (strpos($plan, 'Student') === false && ($source == 'faveo')) {
                                        $dropdown_array = $endorsmentDropDown_faveo;
                                    } else if (strpos($plan, 'Student') !== false && ($productType == 'travel') && ($source == 'faveo')) {
                                        $dropdown_array = $endorsmentDropDown_faveo + $student_edd;
                                    } else {
                                        if (strpos($plan, 'Student') !== false && ($productType == 'travel')) {
                                            $dropdown_array = $endorsmentDropDown + $student_edd;
                                        } else {
                                            $dropdown_array = $endorsmentDropDown;
                                        }
                                    }
                                    foreach ($dropdown_array as $key => $value) {
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
<?php } ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="50" align="right" class="tdborder">&nbsp;</td>
                        <td class="tdborder">&nbsp;</td>
                        <td class="tdborder">&nbsp;</td>
                        <td class="tdborder">&nbsp;</td>
                        <td class="tdborder">&nbsp;</td>
                        <td class="tdborder">
                            <input name="searchItem"  id="searchItem" type="image" img src="img/search_button.png" onclick="endorsementTest();" border="0" />
                        </td>
                    </tr>
                </table>
                <!-- Endorsement search table end-->

                <table width="959" border="0" cellspacing="10" cellpadding="10" id="policyDetailDataloaderthanku" style="display:none;">
                    <tr>
                        <td height="50" align="center" id="errormsg"></td>

                    </tr>
                </table>
                <table width="959" border="0" cellspacing="0" cellpadding="0" id="policyDetailDataloader" style="display:none;">
                    <tr>
                        <td height="50" align="center"><img src="img/logo_loader.gif" /></td>
                    </tr>
                </table>


                <!-- endorsement fields start -->

                <table width="959" border="0" cellspacing="0" cellpadding="0" id="policyDetailData" style="display:none;">
                    <tr>
                        <td height="50" align="left">Policy Details :</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="935" border="0" cellspacing="0" cellpadding="0" class="whitebox">

                                <!-- policy holder name -->
                                <tr id="policyholder_details" style="display:none" class="policy_fields">
                                    <td colspan="6">
                                        <table>
                                            <tr>
                                                <td width="82" id="holder_first_name_label">&nbsp;&nbsp;&nbsp;First Name: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="policyholder_first_name" AUTOCOMPLETE="OFF" readonly="true" id="policyholder_first_name" class="email_f_2" style="width:168px;" value="" placeholder="First Name" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="policyholder_first_name_value" maxlength="25"  onkeypress="return onlyAlphabets(event, this);"  id="policyholder_first_name_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="82" id="holder_last_name_label">&nbsp;&nbsp;&nbsp;Last Name: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="policyholder_last_name" AUTOCOMPLETE="OFF" readonly="true" id="policyholder_last_name" class="email_f_2" style="width:168px;" value="" placeholder="Last Name" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="policyholder_last_name_value" maxlength="25"  onkeypress="return allowDotWithAlphabets(event, this);"  id="policyholder_last_name_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- policy holder details end-->


                                <!-- insured name start -->
                                <tr id="PolicyInsuredName" style="display:none;" class="policy_fields">
                                    <td colspan="6">
                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td width="110">&nbsp;&nbsp;Insured Name: </td>
                                                            <td>&nbsp;</td>
                                                            <td colspan="4">
                                                                <div class="" id="InsuredNameList"> </div>
                                                            </td>
                                                            <td width="130" align="right" colspan="4">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="insured_detail" style="display:none;">
                                                    <table>
                                                        <tr>
                                                            <td width="82" id="insured_first_name_label">&nbsp;&nbsp;&nbsp;First Name: </td>
                                                            <td width="187" height="45">
                                                                <div class="dropdown_otc">
                                                                    <input type="text" name="insured_first_name" AUTOCOMPLETE="OFF" readonly="true" id="insured_first_name" class="email_f_2" style="width:168px;" value="" placeholder="First Name" />
                                                                </div>
                                                            </td>
                                                            <td width="130" align="right">&nbsp;</td>
                                                            <td width="80" class="normal">Change to</td>
                                                            <td width="187">
                                                                <div class="email_f_white">
                                                                    <input type="text" name="insured_first_name_value"  id="insured_first_name_value" onkeypress="return onlyAlphabets(event, this);" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="82" id="insured_last_name_label">&nbsp;&nbsp;&nbsp;Last Name: </td>
                                                            <td width="187" height="45">
                                                                <div class="dropdown_otc">
                                                                    <input type="text" name="insured_last_name" AUTOCOMPLETE="OFF" readonly="true" id="insured_last_name" class="email_f_2" style="width:168px;" value="" placeholder="Last Name" />
                                                                </div>
                                                            </td>
                                                            <td width="130" align="right">&nbsp;</td>
                                                            <td width="80" class="normal">Change to</td>
                                                            <td width="187">
                                                                <div class="email_f_white">
                                                                    <input type="text" name="insured_last_name_value"  id="insured_last_name_value" onkeypress="return allowDotWithAlphabets(event, this);" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- insured name end -->

                                <!-- address detail start-->
                                <tr id="address_details" style="display:none" class="policy_fields">
                                    <td colspan="6">
                                        <table>
                                            <tr>
                                                <td width="78" id="address1_label">&nbsp;&nbsp;&nbsp;Address1: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="address1" AUTOCOMPLETE="OFF" readonly="true" id="address1" class="email_f_2" style="width:168px;" value="" placeholder="Address1" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="address1_value"  id="address1_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="78" id="address1_label">&nbsp;&nbsp;&nbsp;Address2: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="address2" AUTOCOMPLETE="OFF" readonly="true" id="address2" class="email_f_2" style="width:168px;" value="" placeholder="Address2" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="address2_value"  id="address2_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="78" id="locality_label">&nbsp;&nbsp;&nbsp;Locality: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="locality" AUTOCOMPLETE="OFF" readonly="true" id="locality" class="email_f_2" style="width:168px;" value="" placeholder="Locality" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="locality_value"  id="locality_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="78" id="city_label">&nbsp;&nbsp;&nbsp;City: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="city" AUTOCOMPLETE="OFF" readonly="true" id="city" class="email_f_2" style="width:168px;" value="" placeholder="City" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="city_value"  id="city_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="78" id="state_label">&nbsp;&nbsp;&nbsp;State: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="state" AUTOCOMPLETE="OFF" readonly="true" id="state" class="email_f_2" style="width:168px;" value="" placeholder="State" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="state_value"  id="state_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="78" id="pincode_label">&nbsp;&nbsp;&nbsp;Pin Code: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="pincode" AUTOCOMPLETE="OFF" readonly="true" id="pincode" class="email_f_2" style="width:168px;" value="" placeholder="Pin Code" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="pincode_value"  id="pincode_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- address detail end-->

                                <!-- email detail start --->
                                <tr id="email_details" style="display:none" class="policy_fields">
                                    <td width="192" id="email_label">&nbsp;&nbsp;&nbsp;Email: </td>
                                    <td width="187" height="45">
                                        <div class="dropdown_otc">
                                            <input type="text" name="email" AUTOCOMPLETE="OFF" readonly="true" id="email" class="email_f_2" style="width:168px;" value="" placeholder="Email" />
                                        </div>
                                    </td>
                                    <td width="130" align="right">&nbsp;</td>
                                    <td width="80" class="normal">Change to</td>
                                    <td width="187">
                                        <div class="email_f_white">
                                            <input type="text" name="email_value"  id="email_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                        </div>
                                    </td>
                                </tr>
                                <!-- email detail end -->

                                <!-- phone detail start --->
                                <tr id="phone_details" style="display:none" class="policy_fields">
                                    <td width="192" id="phone_label">&nbsp;&nbsp;&nbsp;Phone No: </td>
                                    <td width="187" height="45">
                                        <div class="dropdown_otc">
                                            <input type="text" name="phoneNo" AUTOCOMPLETE="OFF" readonly="true" id="phoneNo" class="email_f_2" style="width:168px;" value="" placeholder="Phone No. " />
                                        </div>
                                    </td>
                                    <td width="130" align="right">&nbsp;</td>
                                    <td width="80" class="normal">Change to</td>
                                    <td width="187">
                                        <div class="email_f_white">
                                            <input type="text" name="phoneNo_value"  id="phoneNo_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" maxlength="10"  onkeypress="return checkOnlyDigits(event)"/>
                                        </div>
                                    </td>
                                </tr>					
                                <!-- phone detail end -->

                                <!-- pan no start --->
                                <tr id="pan_details" style="display:none" class="policy_fields">
                                    <td width="192" id="pan_label">&nbsp;&nbsp;&nbsp;PAN No: </td>
                                    <td width="187" height="45">
                                        <div class="dropdown_otc">
                                            <input type="text" name="panNo" AUTOCOMPLETE="OFF" readonly="true" id="panNo" class="email_f_2" style="width:168px;" value="" placeholder="PAN No. " />
                                        </div>
                                    </td>
                                    <td width="130" align="right">&nbsp;</td>
                                    <td width="80" class="normal">Change to</td>
                                    <td width="187">
                                        <div class="email_f_white">
                                            <input type="text" name="panNo_value"  id="panNo_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" maxlength="10" />
                                        </div>
                                    </td>
                                </tr>					
                                <!-- pan no end -->

                                <!-- passport no start --->
                                <tr id="passport_details" style="display:none" class="policy_fields">
                                    <td width="192" id="passport_label">&nbsp;&nbsp;&nbsp;Passport No: </td>
                                    <td width="187" height="45">
                                        <div class="dropdown_otc">
                                            <input type="text" name="passportNo" AUTOCOMPLETE="OFF" readonly="true" id="passportNo" class="email_f_2" style="width:168px;" value="" placeholder="Passport No. " />
                                        </div>
                                    </td>
                                    <td width="130" align="right">&nbsp;</td>
                                    <td width="80" class="normal">Change to</td>
                                    <td width="187">
                                        <div class="email_f_white">
                                            <input type="text" name="passportNo_value"  id="passportNo_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                        </div>
                                    </td>
                                </tr>					
                                <!-- passport no end -->


                                <!-- nominee detail start -->
                                <tr id="nominee_detail" style="display:none" class="policy_fields">
                                    <td colspan="6">
                                        <table>
                                            <tr>
                                                <td width="78" id="nominee_title_label">&nbsp;&nbsp;&nbsp;Title: </td>
                                                <td width="187" height="45">
                                                    <div class="">
                                                        <select name="nominee_title" id="nominee_title" disabled style="width:177px;background:rgb(226, 233, 215) none repeat scroll 0 0;height: 22px;" >
                                                            <option value="">Select</option>
                                                            <option value="MR">Mr</option>
                                                            <option value="MS">Ms</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="">
                                                        <select name="nominee_title_value" id="nominee_title_value" readonly="true" style="width:177px;height: 22px;" >
                                                            <option value="MR">Mr</option>
                                                            <option value="MS">Ms</option>
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="87" id="nominee_first_name_label">&nbsp;&nbsp;&nbsp;First Name: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="nominee_first_name"  AUTOCOMPLETE="OFF" readonly="true" id="nominee_first_name" class="email_f_2" style="width:168px;" value="" placeholder="First Name" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="nominee_first_name_value" onkeypress="return onlyAlphabets(event, this);"  id="nominee_first_name_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="87" id="nominee_last_name_label">&nbsp;&nbsp;&nbsp;Last Name: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="nominee_last_name" AUTOCOMPLETE="OFF" readonly="true" id="nominee_last_name" class="email_f_2" style="width:168px;" value="" placeholder="Last Name" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="nominee_last_name_value" onkeypress="return onlyAlphabets(event, this);" id="nominee_last_name_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="78" id="nominee_dob_label">&nbsp;&nbsp;&nbsp;DOB: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="nominee_dob" AUTOCOMPLETE="OFF" disabled id="nominee_dob" class="email_f_2" style="width:168px;" value="" placeholder="Nominee Dob" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="nominee_dob_value"  id="nominee_dob_value"  AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="78" id="nominee_relation_label">&nbsp;&nbsp;&nbsp;Relation: </td>
                                                <td width="187" height="45">
                                                    <div class="">
                                                        <select name="nominee_relation" id="nominee_relation" disabled style="width:177px;background:rgb(226, 233, 215) none repeat scroll 0 0;height: 22px;" >
                                                            <option value="">Select</option>
                                                            <?php
                                                            if (!empty($nominee_relationship)) {

                                                                foreach ($nominee_relationship as $key => $value) {
                                                                    echo "<option value='" . $key . "'>" . $value . "</option>";
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="">
                                                        <select name="nominee_relation_value" id="nominee_relation_value" readonly="true" style="width:177px;height:22px;" >
                                                            <?php
                                                            if (!empty($nominee_relationship)) {
                                                                foreach ($nominee_relationship as $key => $value) {
                                                                    echo "<option value='" . $key . "'>" . $value . "</option>";
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- nominee details end -->

                                <!-- martial status start -->
                                <tr id="martial_stat" style="display:none" class="policy_fields">
                                    <td width="192" id="martial_status_label">&nbsp;&nbsp;&nbsp;Martial Status: </td>
                                    <td width="187" height="45">
                                        <div class="">
                                            <select name="martial_status" id="martial_status" disabled style="width:177px;background:rgb(226, 233, 215) none repeat scroll 0 0;height: 22px;"" >
                                                <option value=''>Select</option>
                                                <option value="Married">Married</option>
                                                <option value="Unmarried">Unmarried</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td width="130" align="right">&nbsp;</td>
                                    <td width="80" class="normal">Change to</td>
                                    <td width="187">
                                        <div class="">
                                            <select name="martial_status_value" id="martial_status_value" readonly="true" style="width:177px;rgb(226, 233, 215) none repeat scroll 0 0;height: 22px;"" >
                                                <option value="Married">Married</option>
                                                <option value="Unmarried">Unmarried</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <!-- martial status end-->


                                <!-- mother maiden name start -->
                                <tr id="maiden_name" style="display:none" class="policy_fields">
                                    <td width="192" id="maiden_label">&nbsp;&nbsp;&nbsp;Mother maiden name : </td>
                                    <td width="187" height="45">
                                        <div class="dropdown_otc">
                                            <input type="text" name="mother_maiden_name" AUTOCOMPLETE="OFF" readonly="true" id="mother_maiden_name" class="email_f_2" style="width:168px;" value="" placeholder="Mother maiden name" />
                                        </div>
                                    </td>
                                    <td width="130" align="right">&nbsp;</td>
                                    <td width="80" class="normal">Change to</td>
                                    <td width="187">
                                        <div class="email_f_white">
                                            <input type="text" name="mother_maiden_value" onkeypress="return onlyAlphabets(event, this);"  id="mother_maiden_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                        </div>
                                    </td>
                                </tr>
                                <!-- mother maiden name end-->


                                <!-- university detail start -->
                                <tr id="university_details" style="display:none" class="policy_fields">
                                    <td colspan="6">
                                        <table>
                                            <tr>
                                                <td width="78" id="university_name_label">&nbsp;&nbsp;&nbsp;Name: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="university_name" AUTOCOMPLETE="OFF" readonly="true" id="university_name" class="email_f_2" style="width:168px;" value="" placeholder="University Name" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="university_name_value"  id="university_name_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="78" id="university_address_label">&nbsp;&nbsp;&nbsp;Address: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="university_address" AUTOCOMPLETE="OFF" readonly="true" id="university_address" class="email_f_2" style="width:168px;" value="" placeholder="University Address" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="university_address_value"  id="university_address_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- university detail end-->


                                <!-- course name start -->
                                <tr id="course_name" style="display:none" class="policy_fields">
                                    <td width="192" id="course_label">&nbsp;&nbsp;&nbsp;Course name: </td>
                                    <td width="187" height="45">
                                        <div class="dropdown_otc">
                                            <input type="text" name="cours_name" AUTOCOMPLETE="OFF" readonly="true" id="cours_name" class="email_f_2" style="width:168px;" value="" placeholder="Course name" />
                                        </div>
                                    </td>
                                    <td width="130" align="right">&nbsp;</td>
                                    <td width="80" class="normal">Change to</td>
                                    <td width="187">
                                        <div class="email_f_white">
                                            <input type="text" name="course_value"  id="course_value" onkeypress="return onlyAlphabets(event, this);" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                        </div>
                                    </td>
                                </tr>
                                <!-- course name end-->


                                <!-- sponsor detail start -->
                                <tr id="sponsor_detail"  style="display:none" class="policy_fields">  
                                    <td colspan="6">
                                        <table>
                                            <tr>
                                                <td width="78" id="sponsor_name_label">&nbsp;&nbsp;&nbsp;Name:</td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="sponosor_name" AUTOCOMPLETE="OFF" readonly="true" id="sponosor_name" class="email_f_2" style="width:168px;" value="" placeholder="Sponsor Name" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="sponosor_name_value" onkeypress="return onlyAlphabets(event, this);" id="sponosor_name_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="78" id="sponsor_dob_label">&nbsp;&nbsp;&nbsp;DOB: </td>
                                                <td width="187" height="45">
                                                    <div class="dropdown_otc">
                                                        <input type="text" name="sponosor_dob" AUTOCOMPLETE="OFF" disabled id="sponosor_dob" class="email_f_2" style="width:168px;" value="" placeholder="Sponsor Dob" />
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="email_f_white">
                                                        <input type="text" name="sponosor_dob_value"  id="sponosor_dob_value" AUTOCOMPLETE="OFF" class="email_f_2" style="width:168px;"  placeholder="Change To" value="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="78" id="sponsor_relation_label">&nbsp;&nbsp;&nbsp;Relation: </td>
                                                <td width="187" height="45">
                                                    <div class="">
                                                        <select name="sponsor_relation" id="sponsor_relation" disabled style="width: 177px;background: rgb(226, 233, 215) none repeat scroll 0 0;height: 22px;" >
                                                            <option value="">Select</option>
                                                            <?php
                                                            if (!empty($sponsor_relationship)) {
                                                                foreach ($sponsor_relationship as $key => $value) {
                                                                    echo "<option value='" . $key . "'>" . $value . "</option>";
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td width="130" align="right">&nbsp;</td>
                                                <td width="80" class="normal">Change to</td>
                                                <td width="187">
                                                    <div class="">
                                                        <select name="sponsor_relation_value" id="sponsor_relation_value" readonly="true" style="width:177px;background:rgba(0, 0, 0, 0) none repeat scroll 0 0;height: 22px;" >
                                                            <?php
                                                            if (!empty($sponsor_relationship)) {
                                                                foreach ($sponsor_relationship as $key => $value) {
                                                                    echo "<option value='" . $key . "'>" . $value . "</option>";
                                                                }
                                                            }
                                                            ?>

                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- sponsor detail end-->

                                <tr id="case_id">
                                    <td width="92">&nbsp;&nbsp;&nbsp; Case ID:</td>
                                    <td width="187" height="45">
                                        <div class="dropdown_otc">
                                            <input type="text" readonly="true" AUTOCOMPLETE="OFF" name="zreqnum" maxlength="15" id="zreqnum" value="" class="email_f_2" style="width:168px;" placeholder="Case ID" />
                                        </div>
                                    </td>
                                    <td width="130" align="right">&nbsp;</td>
                                    <td width="80" class="normal">&nbsp;</td>
                                    <td width="187">&nbsp;</td>
                                    <td width="280">&nbsp;</td>
                                    <td width="15">&nbsp;</td>
                                </tr>

                                <tr>
                                    <input type ="hidden" name="cust_id" id="cust_id">
                                        <input type ="hidden" name="mem_id" id="mem_id">
                                            <input type ="hidden" name="dep_id" id="dep_id">
                                                <input type="hidden" name="policy_status" id="policy_status" >
                                                    <input type="hidden" name="policy_effective_date" id="policy_effective_date" >
                                                        </tr>

<!--<tr id="name_note" style="display:none;">
 <td colspan="7">
         <span style='margin-left:20px;color:red;font-weight:normal;'>*Note: Only two characters can be altered through online.<br>
         <span style='margin-left:65px;;'> For alterations which entails change in more than two characters. Please contact administrator!</span></span>
 </td>
</tr>-->


                                                        <tr>
                                                            <td width="14">&nbsp;</td>
                                                            <td width="212" height="45">&nbsp; </td>
                                                            <td width="142" align="right">&nbsp;</td>
                                                            <td width="80" class="normal">&nbsp;</td>
                                                            <td align="left" colspan="3" valign="middle" height="60">
                                                                <input name="" onclick="modifyData();" type="image" img src="img/modify.png"  id="modifyButton" border="0" /> 
                                                            </td>
                                                        </tr>

                                                        </table>

                                                        </td>
                                                        </tr>

                                                        </table>

                                                        <!-- endorsement field end -->

                                                        <table width="959" border="0" cellspacing="0" cellpadding="0" id="ViewPDFEmailPDFpolicyDetailDataloaderthanku" style="display:none;">
                                                            <tr>
                                                                <td></td>
                                                            </tr>
                                                        </table>

                                                        <table width="959" border="0" cellspacing="0" cellpadding="0" id="policyDetailDataloaderModify" style="display:none;">
                                                            <tr>
                                                                <td height="50" align="center"><img src="img/logo_loader.gif" /></td>
                                                            </tr>
                                                        </table>


                                                        </div>
                                                        <div class="cl"></div></div>
<?php include 'inc/inc_footer.php'; ?>
                                                        </body>
                                                        </html>
