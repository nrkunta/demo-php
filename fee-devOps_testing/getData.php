<?php
include_once('api/xml2array.php');
include_once("api/domxml-php4-to-php5.php");
include_once("inc/config.php");
include_once("function.php");
include_once('api/api_prop.php');
//CSRF Validation check  
$csrf= sanitize_data(@$_POST['token']);
if(isset($csrf)){
    if($csrf!=$_SESSION["token"]){
        echo $error = "1## CSRF Validation failed";exit;
    }
}
$policyNumber = sanitize_data(@$_POST['policyNumber']);
$endorsmentDropDown = sanitize_data(@$_POST['endorsmentType']);
$customerId_crm = sanitize_data(@$_POST['customerId']);
$deployment = sanitize_data(@$_POST['deployment']);
$case_id = sanitize_data(@$_POST['case_id']);

$emilData = '';
$policy_status = '';

//generate case id for faveo mode only
if (strtolower($deployment) == 'faveo') {
    $zreqnum = get_case_id();
} else if ((strtolower($deployment) == 'propero') || (strtolower($deployment) == 'crm')) {
    $zreqnum = $case_id;
}
$xmlData = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:rel="http://relinterface.insurance.symbiosys.c2lbiz.com"
					xmlns:xsd="http://intf.insurance.symbiosys.c2lbiz.com/xsd">
					<soap:Header/>
					<soap:Body>
					  <rel:getPolicyDetails>
						 <rel:intGetPolicyIO>
							<xsd:policyNum>' . $policyNumber . '</xsd:policyNum>
						 </rel:intGetPolicyIO>
					  </rel:getPolicyDetails>
				   </soap:Body>
				</soap:Envelope>';

file_put_contents("data/renewal/" . $policyNumber . '-' . $endorsmentDropDown . "_Request.xml", $xmlData);
$response = soapReq($xmlData, 'getPolicyDetails');
file_put_contents("data/renewal/" . $policyNumber . '-' . $endorsmentDropDown . "_Response.xml", $response);
$xml = new xml2array($response);
$dataArr = $xml->getResult();
$error = 'Fault';



if (isset($dataArr['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['error-lists']['err-description']['#text'])) {
    if (strpos($dataArr['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['error-lists']['err-description']['#text'], 'Unable to') !== false) {
        $error = "1## Please check the policy status";
    } else {
        $error = "1##" . $dataArr['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['error-lists']['err-description']['#text'];
    }
} else if (isset($resultData['soapenv:Envelope']['soapenv:Body']['soapenv:Fault']['soapenv:Code']['soapenv:Value']['#text'])) {
    $error = '1## Sorry Webservice not available right now.Please try later';
} else {
    $data = @$dataArr['soapenv:Envelope']['soapenv:Body']['ns:getPolicyDetailsResponse']['ns:return']['int-get-renewal-policy-iO']['policy'];

    if (isset($data)) {
        $policy_status = isset($data['policy-status-cd']['#text']) ? $data['policy-status-cd']['#text'] : '';
        $coverType = @$data['list-policy-product-dOList']['cover-type-cd']['#text'];

        if (empty($policy_status)) {
            echo $error = "1## Please check the policy status";
            exit;
        }

        $policy_commencement_dt = get_timeformat(@$data['policy-commencement-dt']['#text'], 'commencement');
        $customer_id = $data['list-party-dOList']['customer-id']['#text'];
        $member_id = $data['list-policy-product-dOList']['list-policy-product-insured-dOList']['member-id']['#text'];
        // check policy status
        if (($policy_status == 'In Force') || ($policy_status == 'Pending New Biz Issue')) {
            $emilData = 'N/A';
            switch ($endorsmentDropDown) {
                case '01': {
                        $firstName1 = $lastName1 = '';
                        if (isset($data['list-party-dOList']['first-name1']['#text']) && !empty($data['list-party-dOList']['first-name1']['#text'])) {
                            $firstName1 = @$data['list-party-dOList']['first-name1']['#text'];
                        }
                        if (isset($data['list-party-dOList']['last-name1']['#text']) && !empty($data['list-party-dOList']['last-name1']['#text'])) {
                            $lastName1 = @$data['list-party-dOList']['last-name1']['#text'];
                        }
                        $emilData = $firstName1 . '|' . $lastName1;
                        break;
                    }

                //email	
                case '12' : {
                        if (isset($data['list-party-dOList']['list-party-email-dOList']['email-address']['#text']) && !empty($data['list-party-dOList']['list-party-email-dOList']['email-address']['#text'])) {
                            $emilData = @$data['list-party-dOList']['list-party-email-dOList']['email-address']['#text'];
                        }
                        break;
                    }

                //phone	
                case '11' : {
                        if (isset($data['list-party-dOList']['list-party-contact-dOList']['contact-num']['#text']) && !empty($data['list-party-dOList']['list-party-contact-dOList']['contact-num']['#text'])) {
                            $emilData = @$data['list-party-dOList']['list-party-contact-dOList']['contact-num']['#text'];
                        }
                        break;
                    }

                //addition of pan no
                case '20': {
                        if (isset($data['list-party-dOList']['party-identity-dOs']['identity-num']['#text']) && !empty($data['list-party-dOList']['party-identity-dOs']['identity-num']['#text'])) {
                            $emilData = @$data['list-party-dOList']['party-identity-dOs']['identity-num']['#text'];
                        }
                        break;
                    }

                //maidanName

                case '16': {
                        if (isset($data['list-party-dOList']['maiden-name']['#text']) && !empty($data['list-party-dOList']['maiden-name']['#text'])) {
                            $emilData = @$data['list-party-dOList']['maiden-name']['#text'];
                        }
                        break;
                    }

                //course name
                case '32': {
                        if (isset($data['list-policy-additional-fields-dOList']['course-details']['#text']) && !empty($data['list-policy-additional-fields-dOList']['course-details']['#text'])) {
                            $emilData = @$data['list-policy-additional-fields-dOList']['course-details']['#text'];
                        }
                        break;
                    }

                //university detail
                case '31': {
                        $university_name = $university_address = '';
                        if (isset($data['list-policy-additional-fields-dOList']['field8']['#text']) && !empty($data['list-policy-additional-fields-dOList']['field8']['#text'])) {
                            $university_name = @$data['list-policy-additional-fields-dOList']['field8']['#text'];
                        }
                        if (isset($data['list-policy-additional-fields-dOList']['university-address']['#text']) && !empty($data['list-policy-additional-fields-dOList']['university-address']['#text'])) {
                            $university_address = @$data['list-policy-additional-fields-dOList']['university-address']['#text'];
                        }
                        $emilData = $university_name . '|' . $university_address;
                        break;
                    }

                //sponsor detail
                case '30': {
                        $sponsor_name = $sponsor_dob = $sponsor_rel = '';
                        if (isset($data['list-policy-additional-fields-dOList']['sponsor-name']['#text']) && !empty($data['list-policy-additional-fields-dOList']['sponsor-name']['#text'])) {
                            $sponsor_name = @$data['list-policy-additional-fields-dOList']['sponsor-name']['#text'];
                        }
                        if (isset($data['list-policy-additional-fields-dOList']['sponsor-dOB']['#text']) && !empty($data['list-policy-additional-fields-dOList']['sponsor-dOB']['#text'])) {
                            $sponsor_dob = get_timeformat(@$data['list-policy-additional-fields-dOList']['sponsor-dOB']['#text'], 'sponsor');
                        }
                        if (isset($data['list-policy-additional-fields-dOList']['relationship-to-student']['#text']) && !empty($data['list-policy-additional-fields-dOList']['relationship-to-student']['#text'])) {
                            $sponsor_rel = @$data['list-policy-additional-fields-dOList']['relationship-to-student']['#text'];
                        }
                        $emilData = $sponsor_name . '|' . $sponsor_dob . '|' . $sponsor_rel;
                        break;
                    }

                //martial status marital-status-cd
                case '23': {
                        if (isset($data['list-party-dOList']['marital-status-cd']['#text']) && !empty($data['list-party-dOList']['marital-status-cd']['#text'])) {
                            $emilData = @$data['list-party-dOList']['marital-status-cd']['#text'];
                        }
                        break;
                    }

                //nominee detail
                case '05': {
                        $nominee_title = $nominee_first_name = $nominee_last_name = $nominee_dob = $nominee_relation = '';
                        if (isset($data['list-policy-additional-fields-dOList']['field9']['#text']) && !empty($data['list-policy-additional-fields-dOList']['field9']['#text'])) {
                            $nominee_title = @$data['list-policy-additional-fields-dOList']['field9']['#text'];
                        }
                        if (isset($data['list-policy-additional-fields-dOList']['field10']['#text']) && !empty($data['list-policy-additional-fields-dOList']['field10']['#text'])) {
                            $nominee_first_name = @$data['list-policy-additional-fields-dOList']['field10']['#text'];
                        }
                        if (isset($data['list-policy-additional-fields-dOList']['field11']['#text']) && !empty($data['list-policy-additional-fields-dOList']['field11']['#text'])) {
                            $nominee_last_name = @$data['list-policy-additional-fields-dOList']['field11']['#text'];
                        }
                        if (isset($data['list-policy-additional-fields-dOList']['field17']['#text']) && !empty($data['list-policy-additional-fields-dOList']['field17']['#text'])) {
                            $nominee_dob = !empty($data['list-policy-additional-fields-dOList']['field17']['#text']) ? get_timeformat(@$data['list-policy-additional-fields-dOList']['field17']['#text'], 'nominee') : '';
                        }
                        if (isset($data['list-policy-additional-fields-dOList']['field12']['#text']) && !empty($data['list-policy-additional-fields-dOList']['field12']['#text'])) {
                            $nominee_relation = @$data['list-policy-additional-fields-dOList']['field12']['#text'];
                        }
                        $emilData = $nominee_title . '|' . $nominee_first_name . '|' . $nominee_last_name . '|' . $nominee_dob . '|' . $nominee_relation;
                        break;
                    }

                //address

                case '08': {

                        $address1 = $address2 = $city = $locality = $state = $pincode = 'N/A';
                        if (isset($data['list-party-dOList']['list-party-address-dOList'][0]['address-line1-lang1']['#text']) && !empty($data['list-party-dOList']['list-party-address-dOList'][0]['address-line1-lang1']['#text'])) {
                            $add1 = @$data['list-party-dOList']['list-party-address-dOList'][0]['address-line1-lang1']['#text'];
                            /* checking for the value is contains | and replacing with - as a part of address issue-3650 */
                            $address1 = str_replace(array("|", ":"), "-", $add1);
                        }
                        if (isset($data['list-party-dOList']['list-party-address-dOList'][0]['address-line2-lang1']['#text']) && !empty($data['list-party-dOList']['list-party-address-dOList'][0]['address-line2-lang1']['#text'])) {
                            $add2 = @$data['list-party-dOList']['list-party-address-dOList'][0]['address-line2-lang1']['#text'];
                            /* checking for the value is contains | and replacing with - as a part of address issue-3650 */
                            $address2 = str_replace(array("|", ":"), "-", $add2);
                        }
                        if (isset($data['list-party-dOList']['list-party-address-dOList'][0]['city-cd']['#text']) && !empty($data['list-party-dOList']['list-party-address-dOList'][0]['city-cd']['#text'])) {
                            $city1 = @$data['list-party-dOList']['list-party-address-dOList'][0]['city-cd']['#text'];
                            /* checking for the value is contains | and replacing with - as a part of address issue-3650 */
                            $city = str_replace(array("|", ":"), "-", $city1);
                        }
                        if (isset($data['list-party-dOList']['list-party-address-dOList'][0]['area-cd']['#text']) && !empty($data['list-party-dOList']['list-party-address-dOList'][0]['area-cd']['#text'])) {
                            $locality1 = @$data['list-party-dOList']['list-party-address-dOList'][0]['area-cd']['#text'];
                            /* checking for the value is contains | and replacing with - as a part of address issue-3650 */
                            $locality = str_replace(array("|", ":"), "-", $locality1);
                        }
                        if (isset($data['list-party-dOList']['list-party-address-dOList'][0]['state-cd']['#text']) && !empty($data['list-party-dOList']['list-party-address-dOList'][0]['state-cd']['#text'])) {
                            $state1 = @$data['list-party-dOList']['list-party-address-dOList'][0]['state-cd']['#text'];
                            /* checking for the value is contains | and replacing with - as a part of address issue-3650 */
                            $state = str_replace(array("|", ":"), "-", $state1);
                        }
                        if (isset($data['list-party-dOList']['list-party-address-dOList'][0]['pin-code']['#text']) && !empty($data['list-party-dOList']['list-party-address-dOList'][0]['pin-code']['#text'])) {
                            $pincode1 = @$data['list-party-dOList']['list-party-address-dOList'][0]['pin-code']['#text'];
                            /* checking for the value is contains | and replacing with - as a part of address issue-3650 */
                            $pincode = str_replace("|", "-", $pincode1);
                        }
                        $emilData = $address1 . '|' . $address2 . '|' . $city . '|' . $locality . '|' . $state . '|' . $pincode;
                        ;
                        break;
                    }


                // insurred name
                case '15': {
                        $emilData = '';
                        if ($deployment == "crm") {
                            if ($customerId_crm == $data['list-party-dOList'][0]['customer-id']['#text']) {
                                $firstName1 = @$data['list-party-dOList'][0]['first-name1']['#text'];
                                $lastName1 = @$data['list-party-dOList'][0]['last-name1']['#text'];
                                if ($coverType == 'INDIVIDUAL') {
                                    if ($data['list-party-dOList'][0]['customer-id']['#text'] == @$data['list-policy-product-dOList']['list-policy-product-insured-dOList']['customer-id']['#text']) {
                                        $dependentId_wt = @$data['list-policy-product-dOList']['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $customerId_wt = @$data['list-policy-product-dOList']['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $memberId_wt = @$data['list-policy-product-dOList']['list-policy-product-insured-dOList']['member-id']['#text'];
                                    } else {
                                        $dependentId_wt = @$data['list-policy-product-dOList'][0]['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $customerId_wt = @$data['list-policy-product-dOList'][0]['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $memberId_wt = @$data['list-policy-product-dOList'][0]['list-policy-product-insured-dOList']['member-id']['#text'];
                                    }
                                } else {
                                    if ($data['list-party-dOList'][0]['customer-id']['#text'] == @$data['list-policy-product-dOList']['list-policy-product-insured-dOList'][0]['customer-id']['#text']) {
                                        $customerId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][0]['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][0]['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][0]['member-id']['#text'];
                                    } else {
                                        $customerId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList']['member-id']['#text'];
                                    }
                                }
                                $emilData .= $firstName1 . '-' . $lastName1 . '_' . $customerId_wt . 'DI' . $dependentId_wt . '@@@@' . $memberId_wt;
                            } else if ($customerId_crm == $data['list-party-dOList'][1]['customer-id']['#text']) {
                                $firstName1 = @$data['list-party-dOList'][1]['first-name1']['#text'];
                                $lastName1 = @$data['list-party-dOList'][1]['last-name1']['#text'];
                                if ($coverType == 'INDIVIDUAL') {
                                    if ($data['list-party-dOList'][1]['customer-id']['#text'] == @$data['list-policy-product-dOList'][1]['list-policy-product-insured-dOList']['customer-id']['#text']) {
                                        $dependentId_wt = @$data['list-policy-product-dOList'][1]['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $customerId_wt = @$data['list-policy-product-dOList'][1]['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $memberId_wt = @$data['list-policy-product-dOList'][1]['list-policy-product-insured-dOList']['member-id']['#text'];
                                    } else {
                                        $dependentId_wt = @$data['list-policy-product-dOList'][0]['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $customerId_wt = @$data['list-policy-product-dOList'][0]['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $memberId_wt = @$data['list-policy-product-dOList'][0]['list-policy-product-insured-dOList']['member-id']['#text'];
                                    }
                                } else {
                                    if ($data['list-party-dOList'][1]['customer-id']['#text'] == @$data['list-policy-product-dOList']['list-policy-product-insured-dOList'][1]['customer-id']['#text']) {
                                        $customerId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][1]['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][1]['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][1]['member-id']['#text'];
                                    } else {
                                        $customerId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][0]['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][0]['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][0]['member-id']['#text'];
                                    }
                                }
                                $emilData .= $firstName1 . '-' . $lastName1 . '_' . $customerId_wt . 'DI' . $dependentId_wt . '@@@@' . $memberId_wt;
                            } else if ($customerId_crm == $data['list-party-dOList'][2]['customer-id']['#text']) {
                                $firstName1 = @$data['list-party-dOList'][2]['first-name1']['#text'];
                                $lastName1 = @$data['list-party-dOList'][2]['last-name1']['#text'];
                                if ($coverType == 'INDIVIDUAL') {
                                    if ($data['list-party-dOList'][2]['customer-id']['#text'] == @$data['list-policy-product-dOList'][2]['list-policy-product-insured-dOList']['customer-id']['#text']) {
                                        $dependentId_wt = @$data['list-policy-product-dOList'][2]['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $customerId_wt = @$data['list-policy-product-dOList'][2]['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $memberId_wt = @$data['list-policy-product-dOList'][2]['list-policy-product-insured-dOList']['member-id']['#text'];
                                    } else {
                                        $dependentId_wt = @$data['list-policy-product-dOList'][1]['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $customerId_wt = @$data['list-policy-product-dOList'][1]['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $memberId_wt = @$data['list-policy-product-dOList'][1]['list-policy-product-insured-dOList']['member-id']['#text'];
                                    }
                                } else {
                                    if ($data['list-party-dOList'][2]['customer-id']['#text'] == @$data['list-policy-product-dOList']['list-policy-product-insured-dOList'][2]['customer-id']['#text']) {
                                        $customerId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][2]['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][2]['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][2]['member-id']['#text'];
                                    } else {
                                        $customerId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][1]['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][1]['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][1]['member-id']['#text'];
                                    }
                                }
                                $emilData .= $firstName1 . '-' . $lastName1 . '_' . $customerId_wt . 'DI' . $dependentId_wt . '@@@@' . $memberId_wt;
                            } else if ($customerId_crm == $data['list-party-dOList'][3]['customer-id']['#text']) {
                                $firstName1 = @$data['list-party-dOList'][3]['first-name1']['#text'];
                                $lastName1 = @$data['list-party-dOList'][3]['last-name1']['#text'];
                                if ($coverType == 'INDIVIDUAL') {
                                    if ($data['list-party-dOList'][3]['customer-id']['#text'] == @$data['list-policy-product-dOList'][3]['list-policy-product-insured-dOList']['customer-id']['#text']) {
                                        $dependentId_wt = @$data['list-policy-product-dOList'][3]['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $customerId_wt = @$data['list-policy-product-dOList'][3]['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $memberId_wt = @$data['list-policy-product-dOList'][3]['list-policy-product-insured-dOList']['member-id']['#text'];
                                    } else {
                                        $dependentId_wt = @$data['list-policy-product-dOList'][2]['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $customerId_wt = @$data['list-policy-product-dOList'][2]['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $memberId_wt = @$data['list-policy-product-dOList'][2]['list-policy-product-insured-dOList']['member-id']['#text'];
                                    }
                                } else {
                                    if ($data['list-party-dOList'][3]['customer-id']['#text'] == @$data['list-policy-product-dOList']['list-policy-product-insured-dOList'][3]['customer-id']['#text']) {
                                        $customerId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][3]['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][3]['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][3]['member-id']['#text'];
                                    } else {
                                        $customerId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][2]['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][2]['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][2]['member-id']['#text'];
                                    }
                                }
                                $emilData .= $firstName1 . '-' . $lastName1 . '_' . $customerId_wt . 'DI' . $dependentId_wt . '@@@@' . $memberId_wt;
                            } else if ($customerId_crm == $data['list-party-dOList'][4]['customer-id']['#text']) {
                                $firstName1 = @$data['list-party-dOList'][4]['first-name1']['#text'];
                                $lastName1 = @$data['list-party-dOList'][4]['last-name1']['#text'];
                                if ($coverType == 'INDIVIDUAL') {
                                    if ($data['list-party-dOList'][4]['customer-id']['#text'] == @$data['list-policy-product-dOList'][4]['list-policy-product-insured-dOList']['customer-id']['#text']) {
                                        $dependentId_wt = @$data['list-policy-product-dOList'][4]['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $customerId_wt = @$data['list-policy-product-dOList'][4]['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $memberId_wt = @$data['list-policy-product-dOList'][4]['list-policy-product-insured-dOList']['member-id']['#text'];
                                    } else {
                                        $dependentId_wt = @$data['list-policy-product-dOList'][3]['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $customerId_wt = @$data['list-policy-product-dOList'][3]['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $memberId_wt = @$data['list-policy-product-dOList'][3]['list-policy-product-insured-dOList']['member-id']['#text'];
                                    }
                                } else {
                                    if ($data['list-party-dOList'][4]['customer-id']['#text'] == @$data['list-policy-product-dOList']['list-policy-product-insured-dOList'][4]['customer-id']['#text']) {
                                        $customerId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][4]['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][4]['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][4]['member-id']['#text'];
                                    } else {
                                        $customerId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][3]['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][3]['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][3]['member-id']['#text'];
                                    }
                                }
                                $emilData .= $firstName1 . '-' . $lastName1 . '_' . $customerId_wt . 'DI' . $dependentId_wt . '@@@@' . $memberId_wt;
                            }
                        } else {
                            if ($data['list-policy-product-dOList']['cover-type-cd']['#text'] == 'INDIVIDUAL') {
                                for ($i = 0; $i <= 5; $i++) {
                                    if (isset($data['list-party-dOList'][$i]['customer-id']['#text'])) {
                                        $customerId_wt = $data['list-policy-product-dOList'][$i]['list-policy-product-insured-dOList']['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList'][$i]['list-policy-product-insured-dOList']['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList'][$i]['list-policy-product-insured-dOList']['member-id']['#text'];
                                        $customerId = $data['list-party-dOList'][$i]['customer-id']['#text'];
                                        if ($customerId == $data['list-policy-product-dOList'][$i]['list-policy-product-insured-dOList']['customer-id']['#text']) {
                                            $emilData .= $data['list-party-dOList'][$i]['first-name1']['#text'] . '-' . $data['list-party-dOList'][$i]['last-name1']['#text'] . '_' . $customerId_wt . 'DI' . $dependentId_wt . '@@@@' . $memberId_wt . '|';
                                        } else {
                                            if ($i == 0) {
                                                $emilData .= $data['list-party-dOList'][$i]['first-name1']['#text'] . '-' . $data['list-party-dOList'][$i]['last-name1']['#text'] . '_' . $data['list-policy-product-dOList']['list-policy-product-insured-dOList']['customer-id']['#text'] . 'DI' . $data['list-policy-product-dOList']['list-policy-product-insured-dOList']['dependent-id']['#text'] . '@@@@' . $data['list-policy-product-dOList']['list-policy-product-insured-dOList']['member-id']['#text'] . '|';
                                            } else {
                                                $emilData .= $data['list-party-dOList'][$i]['first-name1']['#text'] . '-' . $data['list-party-dOList'][$i]['last-name1']['#text'] . '_' . $data['list-policy-product-dOList'][$i - 1]['list-policy-product-insured-dOList']['customer-id']['#text'] . 'DI' . $data['list-policy-product-dOList'][$i - 1]['list-policy-product-insured-dOList']['dependent-id']['#text'] . '@@@@' . $data['list-policy-product-dOList'][$i - 1]['list-policy-product-insured-dOList']['member-id']['#text'] . '|';
                                            }
                                        }
                                    }
                                }
                            } else {
                                for ($i = 0; $i <= 5; $i++) {
                                    if (isset($data['list-party-dOList'][$i]['customer-id']['#text'])) {
                                        $customerId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][$i]['customer-id']['#text'];
                                        $dependentId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][$i]['dependent-id']['#text'];
                                        $memberId_wt = $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][$i]['member-id']['#text'];
                                        $customerId = $data['list-party-dOList'][$i]['customer-id']['#text'];
                                        if ($customerId == $customerId_wt) {
                                            $emilData .= $data['list-party-dOList'][$i]['first-name1']['#text'] . '-' . $data['list-party-dOList'][$i]['last-name1']['#text'] . '_' . $customerId_wt . 'DI' . $dependentId_wt . '@@@@' . $memberId_wt . '|';
                                        } else {
                                            if ($i == 0) {
                                                $emilData .= $data['list-party-dOList'][$i]['first-name1']['#text'] . '-' . $data['list-party-dOList'][$i]['last-name1']['#text'] . '_' . $data['list-policy-product-dOList']['list-policy-product-insured-dOList']['customer-id']['#text'] . 'DI' . $data['list-policy-product-dOList']['list-policy-product-insured-dOList']['dependent-id']['#text'] . '@@@@' . $data['list-policy-product-dOList']['list-policy-product-insured-dOList']['member-id']['#text'] . '|';
                                            } else {
                                                $emilData .= $data['list-party-dOList'][$i]['first-name1']['#text'] . '-' . $data['list-party-dOList'][$i]['last-name1']['#text'] . '_' . $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][$i - 1]['customer-id']['#text'] . 'DI' . $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][$i - 1]['dependent-id']['#text'] . '@@@@' . $data['list-policy-product-dOList']['list-policy-product-insured-dOList'][$i - 1]['member-id']['#text'] . '|';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    }
            }

            if ($endorsmentDropDown != '15') {
                $output = $emilData . ':' . $customer_id . ':' . trim($endorsmentDropDown) . ':' . $zreqnum . ':' . $policy_status . ':' . $policy_commencement_dt . ':' . $member_id;
            } else {
                $output = $emilData . ':' . $customer_id . ':' . trim($endorsmentDropDown) . ':' . $zreqnum . ':' . $policy_status . ':' . $policy_commencement_dt;
            }
        } else {
            $error = "1## You can't endorse '" . $policy_status . "' Policy. Please contact to administrator.";
        }
    } else {
        $error = "1## Fault";
    }
    //need to get customer id and policy status.
}
if (!empty($output)) {
    echo "2##" . $output;
} else {
    echo $error;
}
?>
