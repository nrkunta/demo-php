<?php 
include_once "inc/db_config.php";
include_once "inc/pagination.php";

@ob_start();
@session_start();


//For page check from session
if(isset($_SESSION['admin_id'])=="")
{
 @header("location:index.php");
} 

$from_date = "";
$to_date = "";
$policyNumber = "";
$endorsmentType = "";
$sourceType = "";
$zreqnum = "";
$report_arr_list = array();
$cond = "";
$source_dropdown =  getSource('endorsement_reports');
$source_dropdown =  array_filter($source_dropdown);


if(isset($_REQUEST['search'])){
    // CSRF Token Validation 
    if(isset($_REQUEST['csrf'])){
        if($_SESSION['token']!=admin_sanitize_data($_REQUEST['csrf'])){
            echo 'CSRF Validation failed';exit;
        }
    }
         unset($_SESSION['from_date']);
         unset($_SESSION['to_date']);
         unset($_SESSION['policyNumber']);
         unset($_SESSION['endorsmentType']);
         unset($_SESSION['sourceType']);
         unset($_SESSION['zreqnum']);
         
         // set session
         // set query condition variable
    
     $_SESSION['from_date'] = $from_date = admin_sanitize_data($_REQUEST['from_date']);
	 $_SESSION['to_date'] = $to_date   = admin_sanitize_data($_REQUEST['to_date']);
	 $_SESSION['policyNumber'] = $policyNumber = admin_sanitize_data($_REQUEST['policyNumber']);
	 $_SESSION['endorsmentType'] = $endorsmentType = admin_sanitize_data($_REQUEST['endorsmentType']);
	 $_SESSION['sourceType'] = $sourceType = admin_sanitize_data($_REQUEST['sourceType']);
	 $_SESSION['zreqnum'] = $zreqnum = admin_sanitize_data($_REQUEST['zreqnum']);
	 
	 $cond = "";
	 // adding a new condition to select only with from date - PROP-3650
	 if($from_date!="" && $to_date==""){
		
		 $fdate = date("Y-m-d",strtotime($from_date));
	   $tdate = date("Y-m-d",strtotime($to_date)); 
	   $todate = date('Y-m-d',strtotime($tdate . "+1 days"));
	   $cond .= " AND (endorsmentDate LIKE '".$fdate."%')";
	   
	 }else if($from_date!="" || $to_date!=""){
	   $fdate = date("Y-m-d",strtotime($from_date));
	   $tdate = date("Y-m-d",strtotime($to_date)); 
	   $todate = date('Y-m-d',strtotime($tdate . "+1 days"));
	   $cond .= " AND (endorsmentDate BETWEEN '".$fdate."' AND '".$todate."')";
	 
	 }	
	 if($policyNumber!="")
	 {
	   $cond .= " AND policyNumber='".$policyNumber."'";
	 }
	 if($endorsmentType!="")
	 {
	   $cond .= " AND endorsmentType='".$endorsmentType."'";
	 }
	 if($sourceType!="")
	 {
	   $cond .= " AND source='".$sourceType."'";
	 }
	 
	 if($zreqnum!="")
	 {
	   $cond .= " AND zreqnum='".$zreqnum."'";
	 }
	 
	
}


elseif(isset($_GET['page']) && (isset($_SESSION['from_date']) || isset($_SESSION['to_date']) || isset($_SESSION['policyNumber']) || isset($_SESSION['endorsmentType']) || isset($_SESSION['sourceType']) || isset($_SESSION['zreqnum'])))
{
	
         
         // check session
         if(isset($_SESSION['from_date'])){
             $_REQUEST['from_date'] = $_SESSION['from_date'];
         }
         if(isset($_SESSION['to_date'])){
             $_REQUEST['to_date'] = $_SESSION['to_date'];
         }
         if(isset($_SESSION['policyNumber'])){
             $_REQUEST['policyNumber'] = $_SESSION['policyNumber'];
         }
         if(isset($_SESSION['endorsmentType'])){
             $_REQUEST['endorsmentType'] = $_SESSION['endorsmentType'];
         }
		 if(isset($_SESSION['sourceType'])){
             $_REQUEST['sourceType'] = $_SESSION['sourceType'];
         }
         if(isset($_SESSION['zreqnum'])){
             $_REQUEST['zreqnum'] = $_SESSION['zreqnum'];
         }
    // set query condition variable
         $from_date = admin_sanitize_data($_REQUEST['from_date']);
	 $to_date   = admin_sanitize_data($_REQUEST['to_date']);
	 $policyNumber = admin_sanitize_data($_REQUEST['policyNumber']);
	 $endorsmentType = admin_sanitize_data($_REQUEST['endorsmentType']);
	 $sourceType = admin_sanitize_data($_REQUEST['sourceType']);
	 $zreqnum = admin_sanitize_data($_REQUEST['zreqnum']);
	 
	 $cond = "";
	 if($from_date!="" && $to_date!="")
	 {
	   $fdate = date("Y-m-d",strtotime($from_date));
	   $tdate = date("Y-m-d",strtotime($to_date)); 
	   $todate = date('Y-m-d',strtotime($tdate . "+1 days"));
	   $cond .= " AND (endorsmentDate BETWEEN '".$fdate."' AND '".$todate."')";
	 }	
	 if($policyNumber!="")
	 {
	   $cond .= " AND policyNumber='".$policyNumber."'";
	 }
	 if($endorsmentType!="")
	 {
	   $cond .= " AND endorsmentType='".$endorsmentType."'";
	 }
	 if($sourceType!="")
	 {
	   $cond .= " AND source='".$sourceType."'";
	 }
	 if($zreqnum!="")
	 {
	   $cond .= " AND zreqnum='".$zreqnum."'";
	 }
}
elseif(!isset($_GET['page']) && !isset($_REQUEST['search']))
{
    // unset session
         unset($_SESSION['from_date']);
         unset($_SESSION['to_date']);
         unset($_SESSION['policyNumber']);
         unset($_SESSION['endorsmentType']);
         unset($_SESSION['sourceType']);
         unset($_SESSION['zreqnum']);
         
         // unset query condition variable
         $cond = "";
}

//Start code fot pagingnation
if(isset($_GET["page"]))
 $page = (int)admin_sanitize_data($_GET["page"]);
else
$page = 1;

 $setLimit = PAGELIMIT;
 $pageLimit = abs(($page * $setLimit) - $setLimit);

$report_arr_list=fetchReportList('endorsement_reports',$cond,$pageLimit,$setLimit); 	//function to fetch records
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="Referrer?Policy" value="no?referrer | same?origin"/>
        <title>Religare Endorsement Admin</title>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="css/pagination.css"/>
        <script type="text/javascript" src="js/jquery.min.js?v=1"></script>
        <script type="text/javascript" src="js/jquery-ui.js?v=1"></script>
        <script type="text/javascript" src="js/placeholders.min.js"></script>
        <script type="text/javascript">
	    $(document).ready(function(){
			 $("#from_date").datepicker({
			     changeMonth:true,
				 changeYear:true,
				 yearRange:"-10:+0",
				 dateFormat:"dd-mm-yy"			 
			 });
			 
			 $("#to_date").datepicker({
			     changeMonth:true,
				 changeYear:true,
				 yearRange:"-10:+0",
				 dateFormat:"dd-mm-yy"			 
			 });
	    });
       </script>
		<script type="text/javascript"> 
		function validateSearch()
		{
		 /*if (document.endorsment_search.to_date.value != "" && (document.endorsment_search.from_date.value === "From Date" || document.endorsment_search.from_date.value === ""))          {
			alert("Please enter from date");
			document.endorsment_search.from_date.focus();
			return false;
		  }
		  else if (document.endorsment_search.from_date.value != "" || (document.endorsment_search.to_date.value === "To Date" || document.endorsment_search.to_date.value === ""))          {
			alert("Please enter to date");
			document.endorsment_search.to_date.focus();
			return false;
		  }*/
		  if ((document.endorsment_search.from_date.value != "" && document.endorsment_search.to_date.value != "") && (document.endorsment_search.from_date.value > document.endorsment_search.to_date.value)) {
			alert("To date should be greater than from date");
			document.endorsment_search.to_date.focus();
			return false;
		  }
		  /*else if (document.endorsment_search.policyNumber.value!= "Policy Number" && isNaN(document.endorsment_search.policyNumber.value)) {
			alert("Please enter the policy number in numeric");
			document.endorsment_search.policyNumber.value="";
			document.endorsment_search.policyNumber.focus();
			return false;
		  }
		  else if (document.endorsment_search.policyNumber.value != "Policy Number" && (document.endorsment_search.policyNumber.value.length>8)) {
			alert("Please enter the valid policy number");
			document.endorsment_search.policyNumber.focus();
			return false;
		  }
		  else if (document.endorsment_search.endorsmentType.value === "" || document.endorsment_search.endorsmentType.value ===0) {
			alert("Please select endorsment type");
			document.endorsment_search.endorsmentType.focus();
			return false;
		  }*/
		  return true;
		}
</script>
</head>
<body>
        <?php include "inc/inc_header.php"; ?>
        <div class="mid_container">
            <div class="quoteBoxgreen"> <?php if(isset($_SESSION['admin_id'])!="") { ?> <a href="logout.php" class="admin_logout" title="Logout"><strong>Logout</strong></a><?php } ?></div>
            <div class="quoteBoxgreenBottom">
 				
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <form name="endorsment_search" id="endorsment_search" action="list.php" method="post" autocomplete="off">
                            <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>"/>
			    <tr>
					<td width="11%">From Date : </td>
					<td width="21%"><div class="dropdown_otc">
                        <input type="text" name="from_date" AUTOCOMPLETE="OFF" id="from_date" class="email_f" style="width:168px;" value="<?php if(isset($_REQUEST['from_date'])) { echo admin_sanitize_data($_REQUEST['from_date']); } else { echo ""; }?>" placeholder="From Date"/>
					</div>
					</td>
					<td width="14%">To Date : </td>
					<td width="26%">
					    <div class="dropdown_otc">
							<input type="text" name="to_date" AUTOCOMPLETE="OFF" id="to_date" class="email_f" style="width:168px;" value="<?php if(isset($_REQUEST['to_date'])) { echo admin_sanitize_data($_REQUEST['to_date']); } else { echo ""; }?>" placeholder="To Date"/>
						</div>
					</td>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td height="50">Policy Number : </td>
					<td height="50"><div class="dropdown_otc">
                        <input type="text" name="policyNumber" AUTOCOMPLETE="OFF" id="policyNumber" maxlength="8" class="email_f" style="width:168px;" value="<?php if(isset($_REQUEST['policyNumber'])) { echo admin_sanitize_data($_REQUEST['policyNumber']); } else { echo ""; }?>" placeholder="Policy Number"/>
					</div>
					</td>
					<td height="50">Endorsment Type : </td>
					<td height="50">
					    <div class="dropdown_otc_dd">
						<select class="styled" name="endorsmentType" id="endorsmentType" style="width:235px;">
							<option value="">Select</option>
							<?php foreach ($endorsmentDropDown as $key => $value) { ?>
								<option value="<?php echo $key; ?>" <?php if ($endorsmentType == $key) { ?> selected="selected" <?php } ?>><?php echo $value; ?></option>
							<?php } ?>
						</select></div>
					</td>
					<td height="50" colspan="2">&nbsp;</td>
				</tr>
				<tr>
					
					<td>Case ID : </td>
					<td><div class="dropdown_otc">
                        <input type="text" name="zreqnum" AUTOCOMPLETE="OFF" id="zreqnum" maxlength="20" class="email_f" style="width:168px;" value="<?php if(isset($_REQUEST['zreqnum'])) { echo admin_sanitize_data($_REQUEST['zreqnum']); } else { echo ""; }?>" placeholder="Case ID"/>
					</div>
					</td>
					<td height="50">Source : </td>
					<td height="50">
					    <div class="dropdown_otc_dd">
						<select class="styled" name="sourceType" id="sourceType" style="width:235px;">
							<option value="">Select</option>
							<?php foreach ($source_dropdown as $value) { ?>
								<option value="<?php echo $value; ?>" <?php if ($sourceType == $value) { ?> selected="selected" <?php } ?>><?php echo strtoupper($value); ?></option>
							<?php } ?>
						</select></div>
					</td>
					<td height="50" colspan="2">&nbsp;</td>
				</tr>
				
				<tr>
					<td height="50" colspan="2">&nbsp;</td>
					<td><input name="search"  id="search" type="submit" class="admin_search" value=""/></td>
					<?php 
					if(isset($_REQUEST['from_date'])!="" || isset($_REQUEST['to_date'])!="" || isset($_REQUEST['policyNumber'])!="" || isset($_REQUEST['endorsmentType'])!="" || isset($_REQUEST['zreqnum'])!="") { 
					?>
				
				  <td width="26%"><input name="cancel"  id="cancel" type="button" class="admin_cancel" value="" onclick="window.location='<?php echo SITEURL;?>admin/list.php';"/></td>
					<td colspan="2">&nbsp;</td>
					<?php } else { ?>
					<td width="1%" colspan="3">&nbsp;</td>
					<?php } ?>
				</tr>
				
				<tr>
					<td height="50" align="right" class="tdborder" colspan="7">&nbsp;</td>
				</tr>
				</form>
				
				<tr>
					<td colspan="7" style="padding-top:20px;">&nbsp;</td>
				</tr>
				
				<tr>
					<td colspan="7">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td colspan="7"><a href="download.php" target="_blank"><img height="50" width="100" src="img/download-excell-icon.png"></a></td>
				</tr>
                                                <tr>
                                                    <td colspan="7">&nbsp;</td>
				</tr>
                <tr>
					<th width="5%">SI No.</th>
					<th width="10%">Date</th>
					<th width="9%">Request ID</th>
					<th width="9%">Policy No.</th>
					<th width="9%">Email ID</th>
					<th width="7%">Time</th>
					<th width="12%">Email Delivery Confirm</th>
					<th width="15%">Endorsement Type</th>					
					<th width="8%">Product Type</th>		
					<th width="8%">Source</th>					
					<th width="8%">Report File</th>
					<th width="8%">Agent Id</th>
					
					
				</tr>
                                                <tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<?php 
				if(count($report_arr_list) >0) 
				{
				  $i =1;
				  foreach($report_arr_list as $fetch_details)
				  { 
				    $endorsmentType = $fetch_details['endorsmentType'];
				    $keyMod = @$endorsmentDropDown[$endorsmentType];				    
                                    if($fetch_details['viewDate']!=""){
                                     $date = date("d M Y",strtotime($fetch_details['viewDate']));
					$time = date("h:i:s",strtotime($fetch_details['viewDate']));
                                    }
                                    else{
                                        $date = date("d M Y",strtotime($fetch_details['endorsmentDate']));
					$time = date("h:i:s",strtotime($fetch_details['endorsmentDate']));
                                    }
				?>
				<tr>
						<td style="padding:5px 2px;"><?php echo $i++; ?></td>
						<td style="padding:5px 2px;"><?php echo $date; ?></td>
						<td style="padding:5px 2px;"><?php echo $fetch_details['zreqnum']; ?></td>
						<td style="padding:5px 2px;"><?php echo $fetch_details['policyNumber']; ?></td>
						<td style="padding:5px 2px;"><?php echo $fetch_details['emailId']; ?></td>
						<td style="padding:5px 2px;"><?php echo $time; ?></td>
						<td style="padding:5px 2px;" align="center"><?php if($fetch_details['keyFlag']==1)echo "Yes"; ?></td>
						<td style="padding:5px 2px;"><?php echo $keyMod; ?></td>	
						<td style="padding:5px 2px;"><?php echo !empty($fetch_details['product_type']) ? $fetch_details['product_type'] : 'N/A'; ?></td>
						<td style="padding:5px 2px;"><?php echo !empty($fetch_details['source']) ? $fetch_details['source'] : 'N/A'; ?></td>							
						<td style="padding:5px 2px;">
						<?php 
						$product_type = !empty($fetch_details['product_type']) ? strtolower($fetch_details['product_type']):'';
						if($product_type == 'travel' && $fetch_details['add_status']=='1'){?>
						<a href="<?php echo '../downloadPdf.php?clientNo='.$fetch_details['policyNumber'].' '?>" target="_blank" style="color:#16622b;">Report PDF</a>
						<?php }else if($product_type == 'travel'){ ?>
						<a href="<?php echo '../downloadPdf.php?clientNo='.$fetch_details['policyNumber'].' '?>" target="_blank" style="color:#16622b;">Report PDF</a>
						<?php }else if($product_type == 'health') { ?>
						<a href="<?php echo '../downloadPdfAdmin.php?id='.base64_encode($fetch_details['zreqnum']); ?>" target="_blank" style="color:#16622b;">Report PDF</a>
						<?php }else if($fetch_details['uploadFile']!="" && file_exists('../data/'.$fetch_details['uploadFile'])) { ?>
						<a href="<?php echo SITEURL.'data/'.$fetch_details['uploadFile']; ?>" target="_blank" style="color:#16622b;">Report PDF</a>
						<?php }else{
						echo 'N/A';
						}
						?>
						</td>
						<!-- added extra coloumn agent_id  prop-3650  -->
						<td><?php echo $fetch_details['agent_id']; ?></td>
							
					
				</tr>
                        <?php } } else { ?>
				<tr>
					<td colspan="7" style="padding-top:20px;">No Result Found!</td>
				</tr>
			    <?php } ?>
                                            </table>
                                        </td>
				</tr>
				
				
				
				
				
				<tr>
					<td colspan="7" style="padding-top:20px;">	
					<div class="page_navigation paginationBox fl">
					<?php
					 // Call the Pagination Function to load Pagination.
				     echo displayPaginationBelow($setLimit,$page,$cond);
					?>
				 </div>
				</td> </tr>
				
				
			  </table>
                
            </div>
            <div class="cl"></div></div>
            <?php include "inc/inc_footer.php"; ?>
    </body>
</html>
