<?php
error_reporting(0);
date_default_timezone_set('Asia/Kolkata');
include_once(dirname(dirname(dirname(dirname(__FILE__)))).'\mysqli_global\confiq_mysqli.php');

connection_endorsement();
selectdb_endorsement();

define("SITEURL", "https://feeuat.religarehealthinsurance.com/");
define("WEBSITEURL", "https://rhicluat.religarehealthinsurance.com/");


define('PAGELIMIT',15);

ini_set('session.cookie_secure',0);
	
$endorsmentDropDown = array(
    '01' => "Modification of Policyholder name",
    '15' => "Modification of Insured Name",
    '08' => "Change in Address",
    '12' => "Change in E-mail ID",
    '11' => "Change in Phone No.",
    '20' => "Addition of PAN No.",
    //'21' => "Addition/Modification of Passport Number",
	'05' => "Nominee Details",
	'23' => "Rectification in Marital Status",
	'16' => "Mother Maiden Name",
	'31' => "University Details",
	'32' => "Course Name",
	'30' => "Sponsor Details"
);


function admin_sanitize_data($input_data) {
    $searchArr = array("document", "write", "alert", "%", "@", "$", ";", "+", "|", "#", "<", ">", ")", "(", "'", "\'", ",");
    $input_data = str_replace("script", "", $input_data);
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function admin_sanitize_password($input_data) {
    $searchArr = array("document", "write", "alert", "%", ";", "+", "|", "#", "<", ">", ")", "(", "'", "\'", ",");
    $input_data = str_replace("script", "", $input_data);
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

function admin_sanitize_data_email($input_data) {
    $searchArr = array("document", "write", "alert", "%", "$", ";", "+", "|", "#", "<", ">", ")", "(", "'", "\'");
    $input_data = str_replace("script", "", $input_data);
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

function mysqltoarray($sql){
	//echo $sql;
	$result = mysqlQuery($sql);
	if(!$result){		
		die("cannot execute query $sql".mysqliError());
	}
	$i	=	0;
	$sqlresult	=	array();
	while($row = mysqlFetchAssoc($result)){
		foreach($row as $key => $value ){
			$sqlresult[$i][$key] = $value;
		}
		$i++;
	}
	return $sqlresult;
}

function getSource($tblname){
	$dataArray	=	array();
	$sql		=	"SELECT DISTINCT  `source` FROM ".$tblname." WHERE `status`='1'";
	$result	=	mysqlQuery($sql);
	while($row = mysqlFetchAssoc($result)){
		foreach($row as $key => $value ){
			$dataArray[$value] = $value;
		}
	
	}
	return $dataArray;
}

function fetchAdminDetails($tblname,$username,$password){
	$dataArray	=	array();
	$sql		=	"SELECT `id`,`username` FROM ".$tblname." WHERE `username` = '".$username."' AND `password` = '".$password."'";
	$dataArray	=	mysqltoarray($sql);
	return $dataArray;
}

function fetchReportList($tblname,$condition,$pageLimit,$setLimit){
	$dataArray	=	array();
	$sql		=	"SELECT * FROM ".$tblname." WHERE 1 AND `status`='1' $condition ORDER BY `id` DESC LIMIT $pageLimit,$setLimit";
	$dataArray	=	mysqltoarray($sql);
	return $dataArray;
}
function fetchReportListDownload($tblname,$condition){
	$dataArray	=	array();
	$sql		=	"SELECT * FROM ".$tblname." WHERE 1 AND `status`='1' $condition ORDER BY `id` DESC ";
	$dataArray	=	mysqltoarray($sql);
	return $dataArray;
}
/**
 * Get the data based on zreq number 
 * @param type $tblname
 * @param type $zreqnum
 * @return type
 */
function fetchDataByZreqNum($tblname,$zreqnum){
    $dataArray	=	array();
    $sql		=	"SELECT * FROM ".$tblname." WHERE 1 AND `status`='1' AND zreqnum='".$zreqnum."' ORDER BY `id` DESC ";
    $dataArray	=	mysqltoarray($sql);
    return $dataArray;
}

?>