<?php
include "inc/db_config.php";
@ob_start();
@session_start();
if($_SESSION["token"]==''){
	$_SESSION["token"] = bin2hex(openssl_random_pseudo_bytes(32));
}
if(isset($_SESSION["admin_name"]) && $_SESSION["admin_name"]!=''){
    @header("location:list.php");exit;
}
if(isset($_REQUEST['login']))
{
  //CSRF Validation check  
  $csrf= admin_sanitize_data(@$_POST['csrf']);
  if(isset($csrf)){
      if($csrf!=$_SESSION["token"]){
           echo 'CSRF Validation failed';exit;
      }
  }
  $username = admin_sanitize_data($_REQUEST['username']);
  $password = mysql_real_escape_string(admin_sanitize_password(trim($_REQUEST['password'])));
  $code = admin_sanitize_data(@$_POST['code']);
   
  
  include "securimage/securimage.php";
  $img = new Securimage($code);

 
  $valid = $img->check($code);
  
  if ($valid == true) 
  {
      $array_admin=fetchAdminDetails('endorsement_user',$username,$password); 	//function to fetch records
	  $total_rows = count($array_admin);
	  if($total_rows==1)
	  {
		session_regenerate_id(); 
		$_SESSION['admin_id'] = $array_admin[0]['id']; 
		$_SESSION['admin_name'] = $array_admin[0]['username']; 
		@header("location:list.php");
	  } 
	  else
	  { 
		$err = "Invalid Username/Password";
	  }
  }
  else 
  {
       $err = "Verification Code Incorrect";
  }	  
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="Referrer?Policy" value="no?referrer | same?origin"/>
        <title>Religare Endorsement Admin</title>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
        <script type="text/javascript" src="js/placeholders.min.js"></script>
		<script type="text/javascript">
		function validate()
		{
		  if (document.admin_login.username.value === "" || document.admin_login.username.value === "Username") {
			alert("Please enter username");
			document.admin_login.username.focus();
			return false;
		  }
		  else if (document.admin_login.password.value === "" || document.admin_login.password.value === "Password") {
			alert("Please enter password");
			document.admin_login.password.focus();
			return false;
		  }
		  else if (document.admin_login.code.value === "") {
			alert("Please enter the verification code");
			document.admin_login.code.focus();
			return false;
		  }
		  return true;
		}
		</script>
    </head>
    <body>
        <?php include "inc/inc_header.php"; ?>
        <div class="mid_container">
            <div class="quoteBoxgreenAdmin"></div>
            <div class="quoteBoxgreenBottom">
 
                <form name="admin_login" id="admin_login" action="" method="post" onSubmit="return validate();" autocomplete="off">
                <table width="959" border="0" cellspacing="0" cellpadding="0">
				      <?php 
					   if(isset($err)!="" && $err!="") { 
					  ?>
				        <tr>
                            <td colspan="3" style="color:#FF0000;float:left;"><?php echo $err; ?></td>
                        </tr>
					<?php } ?>	
						
                        <tr>
                            <td width="69" height="0" align="right">Username :</td>
                            <td width="0" height="50">&nbsp;</td>
                            <td width="280">
                                <div class="dropdown_otc">
                                    <input type="text" name="username" autocomplete="off" id="username" class="email_f" style="width:168px;" value="" placeholder="Username" />
                                    <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>"/>
                                </div>
                            </td>
                        </tr>
						
						<tr>
						    <td width="155" align="right">Password :</td>
                            <td width="34">&nbsp;</td>
                            <td width="388">
                                <div class="dropdown_otc">
                                    <input type="password" name="password" autocomplete="off" id="password" class="email_f" style="width:168px;" value="" placeholder="Password" /></div>
                            </td>
						</tr>
						
						<tr>
                            <td width="69" height="0" align="right">Verification Code :</td>
                            <td width="0" height="50">&nbsp;</td>
                            <td width="280">
                                <div class="dropdown_otc">
                                    <input type="text" name="code" autocomplete="off" id="code" class="email_f" style="width:168px;" value="" placeholder="Verification Code" />
                                </div>
                            </td>
                        </tr>
						<tr>
                            <td width="69" height="0" align="right">&nbsp;</td>
                            <td width="0" height="50">&nbsp;</td>
                            <td width="280">
                                    <img id="siimage" class="fl" width="116px" src="securimage/securimage_show.php?sid=<?php //echo md5(time()) ?>" />
                            </td>
                        </tr>
						
                        <tr>
                            <td height="50" align="right" class="tdborder">&nbsp;</td>
                            <td class="tdborder">&nbsp;</td>
                            <td class="tdborder"><input name="login"  id="login" type="submit" class="admin_submit" value=""/></td>
                        </tr>
                   
                </table>
				</form>
                
            </div>
            <div class="cl"></div></div>
            <?php include "inc/inc_footer.php"; ?>
    </body>
</html>

