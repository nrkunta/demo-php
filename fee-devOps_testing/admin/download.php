<?php
/*
 * Author: Animesh, For download report on excell
 */
include_once "inc/db_config.php";
include_once "inc/pagination.php";

@ob_start();
@session_start();


// varibbale declarition 
$from_date = "";
$to_date = "";
$policyNumber = "";
$endorsmentType = "";
$zreqnum = "";
$report_arr_list = array();
$cond = "";
// set query condition variable
if(isset($_SESSION['from_date']) || isset($_SESSION['to_date']) || isset($_SESSION['policyNumber']) || isset($_SESSION['endorsmentType']) || isset($_SESSION['zreqnum']) )
{
    // check session
         if(isset($_SESSION['from_date'])){
             $_REQUEST['from_date'] = $_SESSION['from_date'];
         }
         if(isset($_SESSION['to_date'])){
             $_REQUEST['to_date'] = $_SESSION['to_date'];
         }
         if(isset($_SESSION['policyNumber'])){
             $_REQUEST['policyNumber'] = $_SESSION['policyNumber'];
         }
         if(isset($_SESSION['endorsmentType'])){
             $_REQUEST['endorsmentType'] = $_SESSION['endorsmentType'];
         }
         if(isset($_SESSION['zreqnum'])){
             $_REQUEST['zreqnum'] = $_SESSION['zreqnum'];
         }
    // set query condition variable
         $from_date = admin_sanitize_data($_REQUEST['from_date']);
	 $to_date   = admin_sanitize_data($_REQUEST['to_date']);
	 $policyNumber = admin_sanitize_data($_REQUEST['policyNumber']);
	 $endorsmentType = admin_sanitize_data($_REQUEST['endorsmentType']);
	 $zreqnum = admin_sanitize_data($_REQUEST['zreqnum']);
	 
	 $cond = "";
	 if($from_date!="" && $to_date!="")
	 {
	   $fdate = date("Y-m-d",strtotime($from_date));
	   $tdate = date("Y-m-d",strtotime($to_date)); 
	   $todate = date('Y-m-d',strtotime($tdate . "+1 days"));
	   $cond .= " AND (endorsmentDate BETWEEN '".$fdate."' AND '".$todate."')";
	 }	
	 if($policyNumber!="")
	 {
	   $cond .= " AND policyNumber='".$policyNumber."'";
	 }
	 if($endorsmentType!="")
	 {
	   $cond .= " AND endorsmentType='".$endorsmentType."'";
	 }
	 if($zreqnum!="")
	 {
	   $cond .= " AND zreqnum='".$zreqnum."'";
	 }
}
else{
    // unset session
         unset($_SESSION['from_date']);
         unset($_SESSION['to_date']);
         unset($_SESSION['policyNumber']);
         unset($_SESSION['endorsmentType']);
         unset($_SESSION['zreqnum']);
         
         // unset query condition variable
         $cond = "";
}


// function calling to return db result
$report_arr_list=fetchReportListDownload('endorsement_reports',$cond); 

// code for export into excell
 $file = 'excelFile-'.date("Y-M-D")."-".time().'.xls';
 
 $content = "";
$content .=' <table width="100%" border="2" cellspacing="0" cellpadding="0">
                    <tr>
					<th width="5%">SI No.</th>
                                        <th width="10%">Date</th>
                                        <th width="14%">Request ID</th>
					<th width="9%">Policy No.</th>
                                        <th width="16%">Email ID</th>
                                        <th width="7%">Time</th>
                                        <th width="6%">Email Delivery Confirm</th>
					<th width="15%">Endorsement Type</th>					
					<th width="10%">Agent Id </th>
					
				</tr>
                                             ';
				 if(count($report_arr_list) >0) 
				{
				  $i =1;
				  foreach($report_arr_list as $fetch_details)
				  { 
				    $endorsmentType = $fetch_details['endorsmentType'];
				    $keyMod = @$endorsmentDropDown[$endorsmentType];				    
                                    if($fetch_details['viewDate']!=""){
                                     $date = date("d M Y",strtotime($fetch_details['viewDate']));
					$time = date("h:i:s",strtotime($fetch_details['viewDate']));
                                    }
                                    else{
                                        $date = date("d M Y",strtotime($fetch_details['endorsmentDate']));
					$time = date("h:i:s",strtotime($fetch_details['endorsmentDate']));
                                    }
				    $fetch_details_zreqnum = $fetch_details['zreqnum'];
                                $fetch_details_policyNumber = $fetch_details['policyNumber'];
                                $fetch_details_emailId = $fetch_details['emailId'];
								$fetch_details_agent_id = $fetch_details['agent_id'];
                                $SITEURL = SITEURL;
                                $uploadFile = $SITEURL."data/".$fetch_details['uploadFile'];
                                if($fetch_details['keyFlag']==1){
                                   $keyFlag = "Yes";
                                }
				    $content .='<tr>
					<td style="padding:5px 2px;">'.$i.'</td>
                                        <td style="padding:5px 2px;">'.$date.'</td>
                                        <td style="padding:5px 2px;">'.$fetch_details_zreqnum.'</td>
					<td style="padding:5px 2px;">'.$fetch_details_policyNumber.'</td>
                                        <td style="padding:5px 2px;">'.$fetch_details_emailId.'</td>
                                        <td style="padding:5px 2px;">'.$time.'</td>
                                        <td style="padding:5px 2px;" align="center">'.$keyFlag.'</td>
					<td style="padding:5px 2px;">'.$keyMod.'</td>
					<td style="padding:5px 2px;">'.$fetch_details_agent_id.'</td>
					';
				    $content .='</tr>';
                              $i++;  }
			}
else { 
				$content .='<tr>
					<td colspan="" style="padding-top:20px;">No Result Found!</td>
				</tr>';
			    } 
                                            $content .='</table>';
 
 header("Expires: 0");
 header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
 header("Cache-Control: no-store, no-cache, must-revalidate");
 header("Cache-Control: post-check=0, pre-check=0", false);
 header("Pragma: no-cache");  header("Content-type: application/vnd.ms-excel;charset:UTF-8");
 header('Content-length: '.strlen($content));
 header('Content-disposition: attachment; filename='.basename($file));
 echo $content;
 exit;
